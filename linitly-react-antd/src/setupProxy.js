const { createProxyMiddleware  } = require('http-proxy-middleware');

module.exports = function(app) {
  const API_BASE_URL = process.env.REACT_APP_API_BASE_URL
  app.use(createProxyMiddleware('/api', {
    target: API_BASE_URL,
    secure: false,
    changeOrigin: true,
    pathRewrite: {
      '^/api': ''
    },
  }));
};
