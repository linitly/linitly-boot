import { useRef } from 'react'

import { Row, Input, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import AddOrUpdateUser from './AddOrUpdateUser'

import IBreadcrumb from '@/components/common/IBreadcrumb'
import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import useShowCUModal from '@/components/common/crud/useShowCUModal'

import { URL_INFIX, NAME } from '@/constant/base/user'

const User = () => {

  // 面包屑数组
  const breadcrumbData = ['系统管理', '岗位管理'];
  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '用户名', name: 'username', children: <Input placeholder="输入用户名进行搜索" /> },
    { label: '手机号', name: 'mobileNumber', children: <Input placeholder="输入手机号进行搜索" /> },
    { label: '昵称', name: 'nickname', children: <Input placeholder="输入昵称进行搜索" /> },
  ]
  // 显示表格数据列
  const tableColumns = [
    { title: '用户名', dataIndex: 'username', render: null, others: null },
    { title: '手机号', dataIndex: 'mobileNumber', render: null, others: null },
    { title: '工号', dataIndex: 'jobNumber', render: null, others: null },
    { title: '昵称', dataIndex: 'nickName', render: null, others: null },
    { title: '真实姓名', dataIndex: 'realName', render: null, others: null },
    { title: '邮箱', dataIndex: 'email', render: null, others: null },
    { title: '性别', dataIndex: 'sex', render: showSex, others: null },
  ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef)

  return (
    <div>
      {/* 面包屑导航 */}
      <IBreadcrumb data={breadcrumbData} />
      {/* 条件搜索表单 */}
      <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} />
      {/* 新增按钮 */}
      <Row>
        <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
      </Row>
      {/* 数据表格 */}
      <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} showUModal={showUModal} del={deleteById} delSuccCallback={memoizedFindAll} />
      {/* 添加或修改弹出框   */}
      <AddOrUpdateUser {...modalProperties} getUsers={memoizedFindAll} />
    </div>
  )

  function showSex(text) {
    return text === 1 ? '男' : text === 2 ? '女' : ''
  }
}

export default User
