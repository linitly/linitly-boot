import { useRef } from 'react'
import { formatTime13 } from '@/utils/date'

import { Row, Input, Button, } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import DeptTreeSelect from '@/components/content/DeptTreeSelect'

import IBreadcrumb from '@/components/common/IBreadcrumb'
import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import CUModal from '@/components/common/crud/CUModal'
import useShowCUModal from '@/components/common/crud/useShowCUModal'

import { URL_INFIX, NAME } from '@/constant/base/post'

const Post = () => {

  const formatTime = (text) => formatTime13(text)

  // 面包屑数组
  const breadcrumbData = ['系统管理', '岗位管理'];
  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '岗位名称', name: 'name', children: <Input placeholder="输入岗位名称进行搜索" /> },
    { label: '所属部门', name: 'sysDeptId', children: <DeptTreeSelect showTopNull={false} /> },
  ]
  // 显示表格数据列
  const tableColumns = [
    { title: '岗位名称', dataIndex: 'name', render: null, others: null },
    { title: '创建时间', dataIndex: 'createdTime', render: formatTime, others: null },
    { title: '最后修改时间', dataIndex: 'lastModifiedTime', render: formatTime, others: null },
  ]
  // 添加和修改表单项
  const modalFormItems = [
    { label: '岗位名称', name: 'name', rules: [{required: true, message: '请输入岗位名称'}], children: <Input placeholder="输入岗位名称" /> },
    { label: '所属部门', name: 'sysDeptId', rules: [{required: true, message: '请选择所属部门'}], children: <DeptTreeSelect /> }
  ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef)

  return (
    <div>
      {/* 面包屑导航 */}
      <IBreadcrumb data={breadcrumbData} />
      {/* 条件搜索表单 */}
      <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} />
      {/* 新增按钮 */}
      <Row>
        <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
      </Row>
      {/* 数据表格 */}
      <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} showUModal={showUModal} del={deleteById} delSuccCallback={memoizedFindAll} />
      {/* 添加或修改弹出框 */}
      <CUModal {...modalProperties} urlInfix={URL_INFIX} name={NAME} succCallback={memoizedFindAll} items={modalFormItems} />
    </div>
  )
}

export default Post
