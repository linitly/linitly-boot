import { useState, useRef } from 'react'
import { formatTime13 } from '@/utils/date'

import { Row, Input, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import Empower from './Empower'

import IBreadcrumb from '@/components/common/IBreadcrumb'
import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import CUModal from '@/components/common/crud/CUModal'
import useShowCUModal from '@/components/common/crud/useShowCUModal'

import { URL_INFIX, NAME } from '@/constant/base/role'

const Role = () => {

  const initDrawerProperties = { visible: false, close: null, roleId: null }
  const [ drawerProperties, setDrawerProperties ] = useState(initDrawerProperties)

  const formatTime = (text) => formatTime13(text)

  // 面包屑数组
  const breadcrumbData = ['系统管理', '角色管理'];
  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '角色名', name: 'name', children: <Input placeholder="输入角色名进行搜索" /> },
    { label: '角色代码', name: 'code', children: <Input placeholder="输入角色代码进行搜索" /> },
  ]
  // 显示表格数据列
  const tableColumns = [
    { title: '角色代码', dataIndex: 'code', render: null, others: null },
    { title: '角色名', dataIndex: 'name', render: null, others: null },
    { title: '创建时间', dataIndex: 'createdTime', render: formatTime, others: null },
  ]
  // 添加和修改表单项
  const modalFormItems = [
    { label: '角色代码', name: 'code', rules: [{required: true, message: '请输入角色代码'}], children: <Input placeholder="请输入角色代码" /> },
    { label: '角色名', name: 'name', rules: [{required: true, message: '请输入角色名'}], children: <Input placeholder="请输入角色名" /> },
    { label: '角色描述', name: 'description', rules: [], children: <Input.TextArea rows={4} placeholder="请输入角色描述" /> },
  ]
  // 其他操作:授权操作
  const empowerOperator = [ {name: '授权', icon: null, onClick: showDrawer} ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef)

  return (
    <div>
      {/* 面包屑导航 */}
      <IBreadcrumb data={breadcrumbData} />
      {/* 条件搜索表单 */}
      <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} />
      {/* 新增按钮 */}
      <Row>
        <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
      </Row>
      {/* 数据表格 */}
      <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} showUModal={showUModal} del={deleteById} delSuccCallback={memoizedFindAll} otherOperator={empowerOperator} />
      {/* 添加或修改弹出框   */}
      <CUModal {...modalProperties} urlInfix={URL_INFIX} name={NAME} succCallback={memoizedFindAll} items={modalFormItems} />
      {/* 修改角色权限抽屉 */}
      <Empower {...drawerProperties} />
    </div>
  )


  // 展示角色权限编辑抽屉
  function showDrawer(record) {
    return function () {
      setDrawerProperties({
        visible: true,
        close: closeDrawer,
        roleId: record.id
      })
    }
  }

  // 关闭角色权限编辑抽屉
  function closeDrawer() {
    setDrawerProperties(initDrawerProperties)
  }
}

export default Role
