import { useState, useRef, useCallback } from 'react'

import { Row, Input, Button } from 'antd';
import { PlusOutlined, SettingOutlined } from '@ant-design/icons';

import DictItem from './DictItem'

import IBreadcrumb from '@/components/common/IBreadcrumb'
import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import CUModal from '@/components/common/crud/CUModal'
import useShowCUModal from '@/components/common/crud/useShowCUModal'

import { URL_INFIX, NAME } from '@/constant/base/dict'

const Dict = () => {

  const initDrawerProperties = {visible: false, close: null, dictId: null}
  const [ drawerProperties, setDrawerProperties ] = useState(initDrawerProperties)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoizedCloseDrawer = useCallback(closeDrawer, [])

  // 面包屑数组
  const breadcrumbData = ['系统管理', '字典管理'];
  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '字典名称', name: 'name', children: <Input placeholder="输入字典名称进行搜索" /> },
    { label: '字典编码', name: 'code', children: <Input placeholder="输入字典编码进行搜索" /> },
  ]
  // 显示表格数据列
  const tableColumns = [
    { title: '字典名称', dataIndex: 'name', render: null, others: null },
    { title: '字典编码', dataIndex: 'code', render: null, others: null },
    { title: '描述', dataIndex: 'description', render: null, others: { ellipsis: true } },
  ]
  // 添加和修改表单项
  const modalFormItems = [
    { label: '字典编码', name: 'code', rules: [{required: true, message: '请输入字典编码'}], children: <Input placeholder="请输入字典编码" /> },
    { label: '字典名称', name: 'name', rules: [{required: true, message: '请输入字典名称'}], children: <Input placeholder="请输入字典名称" /> },
    { label: '描述', name: 'description', rules: [], children: <Input.TextArea rows={4} placeholder="请输入字典描述" /> },
  ]
  // 其他操作:配置字典操作
  const settingOperator = [ {name: '配置字典', icon: <SettingOutlined />, onClick: showSettingDrawer} ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef)

  return (
    <div>
      {/* 面包屑导航 */}
      <IBreadcrumb data={breadcrumbData} />
      {/* 条件搜索表单 */}
      <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} />
      {/* 新增按钮 */}
      <Row>
        <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
      </Row>
      {/* 数据表格 */}
      <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} showUModal={showUModal} del={deleteById} delSuccCallback={memoizedFindAll} otherOperator={settingOperator} />
      {/* 添加或修改弹出框   */}
      <CUModal {...modalProperties} urlInfix={URL_INFIX} name={NAME} succCallback={memoizedFindAll} items={modalFormItems} />
      {/* 配置字典抽屉 */}
      <DictItem {...drawerProperties} />
    </div>
  )

  function showSettingDrawer(record) {
    return function() {
      setDrawerProperties({
        visible: true,
        close: memoizedCloseDrawer,
        dictId: record.id
      })
    }
  }

  function closeDrawer() {
    setDrawerProperties(initDrawerProperties)
  }
}

export default Dict
