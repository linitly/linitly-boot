import { memo, useEffect, useRef } from 'react'
import { Drawer, Card, Row, Input, InputNumber, Button, } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import CUModal from '@/components/common/crud/CUModal'
import useShowCUModal from '@/components/common/crud/useShowCUModal'
import { getOperatorColumn } from '@/components/common/crud/operator/operator.column'

import { formatTime13 } from '@/utils/date'

import { URL_INFIX, NAME } from '@/constant/base/dict.item'

const DictItem = ({
  visible,
  close,
  dictId
}) => {

  const formatTime = (text) => formatTime13(text)

  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '名称', name: 'text', children: <Input placeholder="输入名称进行搜索" /> },
  ]
  // 查询表单隐藏项
  const searchFormHiddenItems = [
    { label: '测试', name: 'sysDataDictId', children: <Input />, others: {hidden: true} },
  ]
  // 查询表单初始值
  const searchFormInitialValues = {sysDataDictId: dictId}
  // CU表单初始值
  const CUFormInitialValues = {...searchFormInitialValues, sort: '0'}
  // 显示表格数据列
  const tableColumns = [
    { title: '名称', dataIndex: 'text', render: null, others: {align: 'center'} },
    { title: '字典值', dataIndex: 'value', render: null, others: {align: 'center'} },
    { title: '创建时间', dataIndex: 'createdTime', render: formatTime, others: {align: 'center'} },
  ]
  // 添加和修改表单项
  const modalFormItems = [
    { label: '名称', name: 'text', rules: [{required: true, message: '请输入字典项名称'}], children: <Input placeholder="请输入字典项名称" /> },
    { label: '字典值', name: 'value', rules: [{required: true, message: '请输入字典值'}], children: <Input placeholder="请输入字典值"/> },
    { label: '排序', name: 'sort', rules: [], children: <InputNumber /> },
    // 默认添加时的字段值
    ...searchFormHiddenItems
  ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef, true, false)

  const operatorColumn = getOperatorColumn(showUModal, deleteById, memoizedFindAll, [], {align: 'center'})

  useEffect(() => {
    dictId && memoizedFindAll()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dictId])

  return (
    <Drawer title="字典列表" placement="right" onClose={close} visible={visible} width={700} destroyOnClose >
      <Card>
        {/*  */}
        {/* 条件搜索表单 */}
        <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} colSpan={12}
        initialValues={searchFormInitialValues} hiddenItems={searchFormHiddenItems} />
        {/* 新增按钮 */}
        <Row>
          <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
        </Row>
        {/* 数据表格 */}
        <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} operatorColumn={operatorColumn} />
        {/* 添加或修改弹出框   */}
        <CUModal {...modalProperties} urlInfix={URL_INFIX} name={NAME} succCallback={memoizedFindAll} items={modalFormItems} initialValues={CUFormInitialValues} />
      </Card>
    </Drawer>
  )
}

export default memo(DictItem)
