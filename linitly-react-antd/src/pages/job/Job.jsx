import { useRef } from 'react'
import { postJson } from '@/utils/http/linitly.request.js'
import { formatTime13 } from '@/utils/date'
import { successPrompt } from '@/utils/default.prompt.js'

import { Row, Input, Button, Tag, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import IBreadcrumb from '@/components/common/IBreadcrumb'
import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import CUModal from '@/components/common/crud/CUModal'
import useShowCUModal from '@/components/common/crud/useShowCUModal'

import DictItemTreeSelect from '@/components/content/DictItemTreeSelect'

import { URL_INFIX, NAME } from '@/constant/base/job'

const Job = () => {

  const DICT_ID = 2

  const formatTime = (text) => formatTime13(text)
  const showJobStatusTag = (text) => text === 1 ? <Tag color="cyan">运行中</Tag> : <Tag color="orange">已暂停</Tag>
  const getStartOrPauseOperatorName = (record) => record.status === 1 ? '暂停' : '启动'

  // 面包屑数组
  const breadcrumbData = ['系统管理', '任务管理'];
  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '任务名称', name: 'jobName', children: <Input placeholder="输入任务名称进行搜索" /> },
    { label: '任务类名', name: 'jobClassName', children: <Input placeholder="输入任务类名进行搜索" /> },
    { label: '任务状态', name: 'status', children: <DictItemTreeSelect dictId={DICT_ID} /> },
  ]
  // 显示表格数据列
  const tableColumns = [
    { title: '任务名称', dataIndex: 'jobName', render: null, others: null },
    { title: '任务类名', dataIndex: 'jobClassName', render: null, others: null },
    { title: '任务状态', dataIndex: 'status', render: showJobStatusTag, others: null },
    { title: '创建时间', dataIndex: 'createdTime', render: formatTime, others: null },
  ]
  // 添加和修改表单项
  const modalFormItems = [
    { label: '任务名称', name: 'jobName', rules: [{required: true, message: '请输入任务名称'}], children: <Input placeholder="请输入任务名称" /> },
    { label: '任务类名', name: 'jobClassName', rules: [{required: true, message: '请输入任务类名'}], children: <Input placeholder="请输入任务类名" /> },
    { label: 'corn表达式', name: 'cronExpression', rules: [{required: true, message: '请输入corn表达式'}], children: <Input placeholder="请输入corn表达式"/> },
    { label: '描述', name: 'description', rules: [], children: <Input.TextArea rows={4} placeholder="请输入任务描述"/> },
  ]
  // 其他操作:启动或暂停任务
  const startOrPauseOperator = [ {name: '', icon: null, onClick: startOrPause, getName: getStartOrPauseOperatorName} ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef)

  return (
    <div>
      {/* 面包屑导航 */}
      <IBreadcrumb data={breadcrumbData} />
      {/* 条件搜索表单 */}
      <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} />
      <Row>
        <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
      </Row>
      {/* 数据表格 */}
      <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} showUModal={showUModal} del={deleteById} delSuccCallback={memoizedFindAll} otherOperator={startOrPauseOperator} />
      {/* 添加或修改弹出框 */}
      <CUModal {...modalProperties} urlInfix={URL_INFIX} name={NAME} succCallback={memoizedFindAll} items={modalFormItems}/>
    </div>
  )

  // 启动或恢复一个定时任务
  function startOrPause(record) {
    return function() {
      const tip = record.status === 1 ? '确定要暂停任务吗？' : '确定要启动任务吗？'
      Modal.confirm({
        content: tip,
        onOk: () => {
          postJson(URL_INFIX + `/pause_or_resume/${record.id}`).then(() => {
            record.status === 1 ? successPrompt('暂停成功') : successPrompt('启动成功')
            memoizedFindAll()
          })
        }
      });
    }
  }
}

export default Job
