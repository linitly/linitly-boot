import { useRef } from 'react'
import { formatTime13 } from '@/utils/date'

import { Row, Input, Button, InputNumber } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import DeptTreeSelect from '@/components/content/DeptTreeSelect'

import IBreadcrumb from '@/components/common/IBreadcrumb'
import SearchForm from '@/components/common/crud/SearchForm'
import ITable from '@/components/common/crud/ITable'
import useRD from '@/components/common/crud/useRD'
import CUModal from '@/components/common/crud/CUModal'
import useShowCUModal from '@/components/common/crud/useShowCUModal'

import getDeleteOperator from '@/components/common/crud/operator/delete.operator'
import getUpdateOperator from '@/components/common/crud/operator/update.operator'
import { getOperatorColumnByRender } from '@/components/common/crud/operator/operator.column'

import { URL_INFIX, NAME } from '@/constant/base/dept'

const Dept = () => {

  const formatTime = (text) => formatTime13(text)

  // 面包屑数组
  const breadcrumbData = ['系统管理', '部门管理'];
  // 查询表单ref
  const searchFormRef = useRef()
  // 查询表单项
  const searchFormItems = [
    { label: '部门名称', name: 'name', children: <Input placeholder="输入部门名称进行搜索" /> },
    { label: '上级部门', name: 'parentId', children: <DeptTreeSelect /> },
  ]
  // 显示表格数据列
  const tableColumns = [
    { title: '部门名称', dataIndex: 'name', render: null, others: null },
    { title: '备注', dataIndex: 'remark', render: null, others: { ellipsis: true } },
    { title: '创建时间', dataIndex: 'createdTime', render: formatTime, others: null },
  ]
  // 添加和修改表单项
  const modalFormItems = [
    { label: '部门名称', name: 'name', rules: [{required: true, message: '请输入部门名称'}], children: <Input placeholder="请输入部门名称" /> },
    { label: '上级部门', name: 'parentId', rules: [{required: true, message: '请选择上级部门'}], children: <DeptTreeSelect /> },
    { label: '排序', name: 'sort', rules: [{required: true, message: '请输入排序值'}], children: <InputNumber /> },
    { label: '备注', name: 'remark', rules: [], children: <Input.TextArea rows={4} placeholder="请输入备注" /> },
  ]

  const [ modalProperties, showCModal, showUModal ] = useShowCUModal(NAME)
  const [ tableLoading, pageResult, deleteById, memoizedFindAll ] = useRD(URL_INFIX, searchFormRef, false)

  function operatorColumnRender(_, record) {
    const updateOperator = getUpdateOperator(showUModal, record)
    const deleteOperator = getDeleteOperator(deleteById, memoizedFindAll, record, record.children)
    return [updateOperator, deleteOperator]
  }

  const operatorColumn = getOperatorColumnByRender(operatorColumnRender)

  return (
    <div>
      {/* 面包屑导航 */}
      <IBreadcrumb data={breadcrumbData} />
      {/* 条件搜索表单 */}
      <SearchForm ref={searchFormRef} findAll={memoizedFindAll} items={searchFormItems} />
      <Row>
        <Button type="primary" icon={ <PlusOutlined /> } onClick={showCModal}>新增</Button>
      </Row>
      {/* 数据表格 */}
      <ITable columns={tableColumns} pageResult={pageResult} tableLoading={tableLoading} operatorColumn={operatorColumn} />
      {/* 添加或修改弹出框 */}
      <CUModal {...modalProperties} urlInfix={URL_INFIX} name={NAME} succCallback={memoizedFindAll} items={modalFormItems} initialValues={{ sort: '0' }} />
    </div>
  )
}

export default Dept
