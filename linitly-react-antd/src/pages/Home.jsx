import React, { useState, lazy, Suspense, useRef } from 'react'
import { Route, Switch, useRouteMatch, Redirect } from 'react-router-dom'
import { postJson } from '@/utils/http/linitly.request.js'
import { successPrompt } from '@/utils/default.prompt'

import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';
import './Home.less'

import Logo from '@/components/common/logo/Logo'
import HeadContent from '@/components/common/head_content/HeadContent'
import MenuTree from '@/components/common/MenuTree'
import Loading from '@/components/common/loading/Loading';
import IWebSocket from '@/utils/ws/IWebSocket'

const NotFound = lazy(() => import('@/components/common/not_found/NotFound'));
const User = lazy(() => import('@/pages/user/User'));
const Role = lazy(() => import('@/pages/role/Role'));
const Dept = lazy(() => import('@/pages/dept/Dept'));
const Post = lazy(() => import('@/pages/post/Post'));
const Dict = lazy(() => import('@/pages/dict/Dict'));
const Job = lazy(() => import('@/pages/job/Job'));
const Front = lazy(() => import('@/pages/Front'));

const { Header, Sider, Content } = Layout;

const Home = ({
  history
}) => {

  const [collapsed, setCollapsed] = useState(false)
  const { path } = useRouteMatch()

  const user = JSON.parse(sessionStorage.getItem('user'))
  const wsRef = useRef()

  return (
    !user ? <Redirect to='/login' /> :
    <Layout className="home">
      <IWebSocket ref={wsRef} />
      <Sider width={240} trigger={null} collapsible collapsed={collapsed}>
        <Logo collapsed={collapsed}></Logo>
        <MenuTree menus={user.menuTree} ></MenuTree>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: toggle,
          })}
          <HeadContent avatar={user.headImgUrl} username={user.username} logout={logout} ></HeadContent>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
          }}
        >
          <Suspense fallback={<Loading />}>
            <Switch>
              <Route exact path={path} component={Front} />
              <Route path={`${path}/user`} component={User} />
              <Route path={`${path}/role`} component={Role} />
              <Route path={`${path}/dept`} component={Dept} />
              <Route path={`${path}/post`} component={Post} />
              <Route path={`${path}/dict`} component={Dict} />
              <Route path={`${path}/job`} component={Job} />
              <Route component={NotFound} />
            </Switch>
          </Suspense>
        </Content>
      </Layout>
    </Layout>
  )

  function logout() {
    postJson('/logout').then(() => {
      successPrompt('退出登录成功')
      sessionStorage.removeItem('user')
      sessionStorage.removeItem('lastSelectKeys')
      sessionStorage.removeItem('lastOpenKeys')
      localStorage.removeItem('token')
      localStorage.removeItem('refresh_token')
      // 关闭ws连接
      wsRef && wsRef.current && wsRef.current.close()
      history.replace('/login')
    })
  }

  function toggle() {
    setCollapsed(!collapsed)
  }
}

export default Home
