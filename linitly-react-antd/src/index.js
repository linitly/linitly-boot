import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

import zhCN from 'antd/lib/locale/zh_CN';
import { ConfigProvider } from 'antd'

import { Provider } from 'react-redux'
import store from './redux/store'

ReactDOM.render(
  <Provider store={store}>
    <ConfigProvider locale={zhCN}>
      <App />
    </ConfigProvider>
  </Provider>
  ,
  document.getElementById('root')
)
