// 系统码
const SYSTEM_CODE = '101'

// 查询全部业务数据的请求路由后缀
const FIND_ALL_URL = '/findAll'
// 根据ID查询单条业务数据的请求路由后缀
const FIND_BY_ID_URL = '/findById'
// 添加业务数据的请求路由后缀
const INSERT_URL = '/insert'
// 修改业务数据的请求路由后缀
const UPDATE_BY_ID_URL = '/updateById'
// 删除业务数据的请求路由后缀
const DELETE_BY_ID_URL = '/deleteById'

// 代表新增的type值
const INSERT_TYPE = 1
// 代表修改的type值
const UPDATE_TYPE = 2

export {
  SYSTEM_CODE,
  FIND_ALL_URL,
  FIND_BY_ID_URL,
  INSERT_URL,
  UPDATE_BY_ID_URL,
  DELETE_BY_ID_URL,
  INSERT_TYPE,
  UPDATE_TYPE,
}
