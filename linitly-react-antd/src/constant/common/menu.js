// 默认菜单的icon
const DEFAULT_ICON_TYPE = 'icon-templatedefault'

// 最后选中的菜单key存储键名
const LAST_SELECT_KEYS = 'lastSelectKeys';
// 最后展开菜单key存储键名
const LAST_OPEN_KEYS = 'lastOpenKeys';

export { DEFAULT_ICON_TYPE, LAST_SELECT_KEYS, LAST_OPEN_KEYS }
