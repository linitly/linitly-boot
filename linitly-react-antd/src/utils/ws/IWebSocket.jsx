import { useImperativeHandle, forwardRef } from 'react'
import SingleWebSocket from './SingleWebSocket'

const IWebSocket = (_, ref) => {

  const WS = SingleWebSocket.getInstance()

  useImperativeHandle(ref, () => ({
    close: () => WS.close(),
    send: (type, message) => WS.send(type, message),
  }))

  return(
    <>
    </>
  )
}

export default forwardRef(IWebSocket)
