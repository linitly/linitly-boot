// 心跳消息类型码
const HEART_BEAT_TYPE = 1
// 通用消息类型
const COMMON_TYPE = 200

export { HEART_BEAT_TYPE, COMMON_TYPE }
