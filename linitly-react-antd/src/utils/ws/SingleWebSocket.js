import createWS from './ws.create'

class SingleWebSocket {
  constructor(ws, send, onMessage) {
    this.ws = ws
    this.send = send
    this.onMessage = onMessage
    this.close = () => {
      this.ws.close()
      this.send = null
      this.onMessage = null
      this.ws = null
    }
  }

  static getInstance() {
    if (!this.instance) {
      const [ ws, send, onMessage ] = createWS();
      this.instance = new SingleWebSocket(ws, send, onMessage)
    }
    return this.instance
  }
}

export default SingleWebSocket
