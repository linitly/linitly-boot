class WSResult {
  constructor(type, message) {
    // 消息类型
    this.type = type;
    // 消息体
    this.message = message;
  }
}

export default WSResult
