/**
 * args：业务组件获取所有数据方法的arguments参数
 * pagination: 业务组件存储的pagination对象
 * data：业务组件中查询表单的查询条件对象
 */
function getPaginationRequestParams(args, pagination, data) {
  switch (args.length) {
    case 0:
      // 组件刚进入时的请求，此时没有参数(或具有初始化的参数)
      return {pageNumber: pagination.current, pageSize: pagination.pageSize, data}
    case 1:
      // 查询表单点击查询或重置时，只有一个对象条件参数
      return {pageNumber: pagination.current, pageSize: pagination.pageSize, data: args[0]}
    case 2:
      // pageNumber或者pageSize改变时的请求
      return {pageNumber: args[0], pageSize: args[1], data}
    default:
      break;
  }
}

export { getPaginationRequestParams }
