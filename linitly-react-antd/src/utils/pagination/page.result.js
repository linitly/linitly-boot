import { Pagination, updatePagination } from './pagination'

class PageResult {
  constructor(onChange, usePagination) {
    this.pagination = new Pagination(onChange, usePagination)
    this.data = []
  }
}

function updatePageResult(beforePageResult, responsePageResult) {
  const newPagination = updatePagination(beforePageResult.pagination, responsePageResult.pagination)
  return {
    pagination: newPagination,
    data: responsePageResult.result
  }
}

export { PageResult, updatePageResult }
