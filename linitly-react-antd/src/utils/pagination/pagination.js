const DEFAULT_PAGE_NUMBER = 1;
const DEFAULT_PAGE_SIZE = 10;

class Pagination {
  constructor(onChange, usePagination) {
    this.position = usePagination ? ['bottomRight'] : ['none'];
    this.current = DEFAULT_PAGE_NUMBER;
    this.pageSize = DEFAULT_PAGE_SIZE;
    this.total = 0;
    this.showSizeChanger = true;
    this.onChange = onChange;
    this.showTotal = (total) => `共${total}条`;
  }
}

function updatePagination(beforePagination, responsePagination) {
  return {
    ...beforePagination,
    current: responsePagination.pageNumber,
    pageSize: responsePagination.pageSize,
    total: responsePagination.totalCount
  }
}

export { Pagination, updatePagination }
