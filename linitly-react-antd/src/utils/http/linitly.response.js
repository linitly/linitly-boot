import { errorPrompt, warnPrompt } from "../default.prompt";

const responseCodeDeal = (response) => {

  switch (response.code) {
    case 200:
      return Promise.resolve(response.data)
    case 405:
    case 411:
      warnPrompt(response.message)
      return Promise.reject(response)
    case 412:
    case 417:
      warnPrompt(response.message)
      window.location.href='/login'
      return Promise.reject(response)
    default:
      errorPrompt(response.message || '系统异常，请刷新重试')
      return Promise.reject(response)
  }
}

export default responseCodeDeal
