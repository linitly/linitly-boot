import axios from 'axios';
import { errorPrompt, warnPrompt } from '../default.prompt';

const newInstance = axios.create();

newInstance.defaults.baseURL = '/api';
newInstance.defaults.timeout = 3000;

newInstance.interceptors.request.use(
  (config) => {
    // 一般在axios层的请求正常拦截处理中，不需要做处理
    return config;
  },
  (error) => {
    // 一般很少会进来这里，几乎没有触发的场景，只需要做一个最后的保底提示就可以
    warnPrompt("请求出错，请刷新页面重试");
    return Promise.reject(error);
  }
);

newInstance.interceptors.response.use(
  (response) => {
    // http状态码为2XX，会进入到这里，是axios的默认机制
    return response;
  },
  (error) => {
    // console.log("res error", JSON.stringify(error));
    // console.log(JSON.stringify(error.response))
    // 一般在http状态码错误的时候、取消请求的时候或者请求运行异常的时候会进入到这里
    if (error.response) {
      errorPrompt(codeMessage[error.response.status] || error.response.statusText || '系统异常，请刷新重试');
    }
    if (error.message.includes('timeout')) {
      warnPrompt('服务请求超时');
    }
    if (error.message.includes('Network Error')) {
      errorPrompt('请检查本地的网络');
    }
    return Promise.reject(error);
  }
);

const codeMessage = {
  400: '请求错误(400)',
  401: '未授权，请重新登录(401)',
  403: '拒绝访问(403)',
  404: '请求出错(404)',
  406: '请求格式错误(406)',
  408: '请求超时(408)',
  410: '请求资源不存在(410)',
  422: '验证错误(422)',
  500: '服务器异常(500)',
  502: '网关错误(502)',
  503: '服务不可用(503)',
  504: '网关超时(504)',
  505: 'http版本不支持(505)',
}

export default newInstance;
