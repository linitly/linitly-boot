function formatTime13(timestamp) {
  if (!timestamp) return ''
  timestamp += ''
  if (timestamp.length !== 13) return ''
  var time = new Date(parseInt(timestamp));
  var y = time.getFullYear();               // getFullYear方法以四位数字返回年份
  var M = time.getMonth() + 1;              // getMonth方法从 Date 对象返回月份 (0 ~ 11)，返回结果需要手动加一
  var d = time.getDate();                   // getDate方法从 Date 对象返回一个月中的某一天 (1 ~ 31)
  var h = time.getHours();                  // getHours方法返回 Date 对象的小时 (0 ~ 23)
  var m = time.getMinutes();                // getMinutes方法返回 Date 对象的分钟 (0 ~ 59)
  var s = time.getSeconds();                // getSeconds方法返回 Date 对象的秒数 (0 ~ 59)
  return y + '-' + zeroPadding(M) + '-' + zeroPadding(d) + ' ' + zeroPadding(h) + ':' + zeroPadding(m) + ':' + zeroPadding(s);
}

function formatNow() {
  var now = +new Date()
  return formatTime13(now)
}

function zeroPadding(i) {
  return parseInt(i) < 10 ? '0' + i : i
}

export { formatTime13, formatNow }
