import { message } from 'antd';

const successPrompt = (msg) => {
  // console.log(msg)
  message.success(msg);
}

const errorPrompt = (msg) => {
  // alert(msg)
  message.error(msg)
}

const warnPrompt = (msg) => {
  // alert(msg)
  message.warning(msg)
}

export { successPrompt, errorPrompt, warnPrompt }
