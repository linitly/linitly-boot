import { memo } from "react"
import { Table, Space } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

import DeletePop from '@/components/common/DeletePop'

const OperatorColumn = ({
  // 显示修改的表单Modal
  showUModal,
  // 删除方法，可以接受一个参数为id
  del,
  // 删除之后的成功的回调方法
  delSuccCallback
}) => {

  const { Column } = Table

  return (
    <Column title="操作" dataIndex="operator" key="operator" render={(text, record) => (
      <Space size="middle">
        <a onClick={showUModal(record)}><EditOutlined />&nbsp;编辑</a>
        <DeletePop del={del(record.id)} successCallback={delSuccCallback}>
          <a><DeleteOutlined />&nbsp;删除</a>
        </DeletePop>
      </Space>
    )} />
  )
}

export default memo(OperatorColumn)
