import { memo, useImperativeHandle, forwardRef } from 'react'
import { Form, Row, Col, Button } from 'antd';
import { SearchOutlined, ReloadOutlined } from '@ant-design/icons';

// ref：接受查询表单的ref
const SearchForm = (props, ref) => {

  /**
   * props说明
   * 查询数据方法
   * findAll,
   * 表单项：格式如下{label: '', name: '', children: <组件(input/textarea)>, others: {}}
   * items,
   * 表单初始值
   * initialValues,
   * 列所占栅格数
   * colSpan,
   * 不显示的表单项
   * hiddenItems
   */
  const { findAll, items, initialValues, colSpan, hiddenItems } = props
  const [ searchForm ] = Form.useForm()
  const buttonsCol = (
    <Col key="buttons">
      <Button type="primary" htmlType="submit" icon={<SearchOutlined />}>搜索</Button>
      <Button style={{margin: '0 8px',}} onClick={resetButtonClickHandle} icon={ <ReloadOutlined /> }>重置</Button>
    </Col>
  )
  // 默认一行占的栅格数
  const ROW_GUTTER = 24
  // 默认一列占的栅格数
  const COL_SPAN = colSpan || 5
  // 默认一行放置的表单项数量
  const ROW_ITEMS = colSpan ? parseInt(ROW_GUTTER / COL_SPAN) : 4

  useImperativeHandle(ref, () => ({
    // 获取查询表单的值
    getFormValues: () => searchForm.getFieldsValue(true)
  }))

  return (
    <Form form={searchForm} style={{marginTop: '20px'}} autoComplete="off" onFinish={findAll} initialValues={initialValues} >
      {
        getHiddenItems()
      }
      {
        getItems()
      }
    </Form>
  )

  function getItems() {
    if (!items) return null
    const rows = parseInt(items.length / ROW_ITEMS)
    if (rows === 0) {
      // 小于ROW_ITEMS
      return getFormItemIncompleteRow(items, 0)
    }
    // 大于等于ROW_ITEMS
    const remainder = items.length % ROW_ITEMS
    let formItems = []
    if (remainder === 0) {
      // 余数为0，表单项正好是4的倍数，按钮需要新开一行
      for (let i = 0; i < rows; i++) {
        formItems.push(getFormItemFullRow(i))
      }
      const buttonsRow = (
        <Row key={rows + 1}>{buttonsCol}</Row>
      )
      formItems.push(buttonsRow);
      return formItems
    } else {
      // 余数不为0，按钮不需要新开一行
      for (let i = 0; i < rows; i++) {
        formItems.push(getFormItemFullRow(i))
      }
      const lastRowItems = items.slice(rows * ROW_ITEMS, items.length)
      const lastRow = getFormItemIncompleteRow(lastRowItems, rows * ROW_ITEMS)
      formItems.push(lastRow)
      return formItems
    }
  }

  // 获取表单一个完整行，循环获取对应的表单项，组成一行返回
  function getFormItemFullRow(rowNumber) {
    let cols = []
    for (let j = 0; j < ROW_ITEMS; j++) {
      let index = rowNumber * ROW_ITEMS + j
      let item = items[index]
      cols.push(
        <Col span={COL_SPAN} key={index}>
          <Form.Item label={item.label} name={item.name} {...item.others}>
            {item.children}
          </Form.Item>
        </Col>
      )
    }
    return (
      <Row gutter={ROW_GUTTER} key={rowNumber}>
        {cols}
      </Row>
    )
  }

  // 获取表单的不完整行，最后补充上按钮列，组成一行返回
  function getFormItemIncompleteRow(lastRowItems, beginIndex) {
    const formItems = lastRowItems.map((item, index) => (
      <Col span={COL_SPAN} key={beginIndex + index}>
        <Form.Item label={item.label} name={item.name} {...item.others}>
          {item.children}
        </Form.Item>
      </Col>
    ))
    return (
      <Row gutter={ROW_GUTTER} key={beginIndex}>
        {[...formItems, buttonsCol]}
      </Row>
    )
  }

  // 获取隐藏的表单项
  function getHiddenItems() {
    if (!hiddenItems || hiddenItems.length < 1) return null
    return hiddenItems.map(item => (
      <Form.Item key={item.name} label={item.label} name={item.name} {...item.others} hidden>
        {item.children}
      </Form.Item>
    ))
  }

  // 重置按钮点击处理
  function resetButtonClickHandle() {
    searchForm.resetFields();
    findAll()
  }
}


export default memo(forwardRef(SearchForm))
