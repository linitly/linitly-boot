import { Form } from "antd"
import { memo } from "react"

const CUForm = ({
  // label所占的栅格数，默认为4，可以不传入
  labelCol,
  // 表单初始值
  initialValues,
  // 表单项：格式为{ label: '', name: '', rules: [], children: <组件(input/textarea...)> }
  items,
  // form表单
  _CUForm
}) => {

  const DEFAULT_COL_SPAN = 4

  return (
    <Form form={_CUForm} autoComplete="off" style={{padding: '24px 24px 0 0'}} labelCol={{ span: labelCol || DEFAULT_COL_SPAN }} preserve={false} initialValues={initialValues}>
      {
        items.map((item, index) =>
          <Form.Item key={index} label={item.label} name={item.name} rules={item.rules} {...item.others}>
            {item.children}
          </Form.Item>
        )
      }
    </Form>
  )
}

export default memo(CUForm)
