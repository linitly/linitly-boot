import { useState, useCallback } from 'react'
import { INSERT_TYPE, UPDATE_TYPE } from '@/constant/common/common'

const useShowCUModal = (
  name
) => {

  // 添加或修改业务数据的Modal表单属性
  const initModalProperties = {title: '', visible: false, type: 0, cancel: null}
  const [ modalProperties, setModalProperties ] = useState(initModalProperties)

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoizedCancelModal = useCallback(cancelModal, [])

  // 显示新增表单Modal
  function showCModal() {
    setModalProperties({
      title: '添加' + name,
      visible: true,
      okFunType: INSERT_TYPE,
      cancel: memoizedCancelModal,
      dataId: null
    })
  }

  // 显示修改表单Modal
  function showUModal(record) {
    return function() {
      setModalProperties({
        title: '修改' + name,
        visible: true,
        okFunType: UPDATE_TYPE,
        cancel: memoizedCancelModal,
        dataId: record.id
      })
    }
  }

  // 取消显示Modal表单的方法
  function cancelModal() {
    setModalProperties(initModalProperties)
  }

  return [ modalProperties, showCModal, showUModal ]
}

export default useShowCUModal
