import { memo } from "react"
import { Modal } from 'antd';

import useCU from '@/components/common/crud/useCU'
import CUForm from '@/components/common/crud/CUForm'
import { INSERT_TYPE, UPDATE_TYPE } from '@/constant/common/common'

// 添加或修改表单Modal
const CUModal = ({
  urlInfix,
  name,
  // 宽度，如果不传入，则默认600px
  width,
  // modal的显示标题
  title,
  // modal是否显示
  visible,
  // modal确认时的方法类型
  okFunType,
  // modal取消方法
  cancel,
  // modal方法执行成功的回调
  succCallback,
  // 数据id，修改时需要
  dataId,
  // 表单项：格式为{ label: '', name: '', rules: [], children: <组件(input/textarea...)> }
  items,
  // label所占的栅格数，默认为4，可以不传入
  labelCol,
  // 表单初始值，可以不传入
  initialValues,
}) => {

  const DEFAULT_WIDTH = 600

  const [ confirmLoading, _CUForm, insert, update ] = useCU(urlInfix, name, dataId, () => {cancel(); succCallback()})
  const okFun = okFunType === INSERT_TYPE ? insert : okFunType === UPDATE_TYPE ? update : null

  return (
    <Modal width={width || DEFAULT_WIDTH} title={title} visible={visible} confirmLoading={confirmLoading} onCancel={cancel}
    onOk={okFun} destroyOnClose>
      <CUForm labelCol={labelCol} items={items} initialValues={initialValues} _CUForm={_CUForm} />
    </Modal>
  )
}

export default memo(CUModal)
