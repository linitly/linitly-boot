import { EditOutlined } from '@ant-design/icons';

function getUpdateOperator(
  // 修改操作点击方法，必传
  func,
  // 方法参数，必传
  record,
  title = '编辑',
  disabled= false,
  useIcon = true,
) {

  return (
    <a key="update_operator" disabled={disabled} onClick={func(record)}>
      {
        useIcon
        ?
        <>
          <EditOutlined />&nbsp;
        </>
        :
        null
      }
      {title}
    </a>
  )
}

export default getUpdateOperator
