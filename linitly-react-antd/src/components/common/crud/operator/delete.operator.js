import { DeleteOutlined } from '@ant-design/icons';

import DeletePop from '@/components/common/DeletePop'

function getDeleteOperator(
  // 删除方法
  del,
  // 删除成功的回调方法
  successCallback,
  // 方法参数，必传
  record,
  disabled= false,
  title = '删除',
  useIcon = true,
) {

  return (
    <>
      {
        disabled
        ?
        getA(disabled)
        :
        <DeletePop del={del(record.id)} successCallback={successCallback}>
          {getA(disabled)}
        </DeletePop>
      }
    </>
  )

  function getA(disabled) {
    return (
      <a key="delete_operator" disabled={disabled}>
        {
          useIcon
          ?
          <>
            <DeleteOutlined />&nbsp;
          </>
          :
          null
        }
        {title}
      </a>
    )
  }
}

export default getDeleteOperator
