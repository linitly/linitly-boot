import { memo } from "react"
import { Table, Empty } from 'antd';

// import getOperatorColumn from '@/utils/operator.column'
import { getOperatorColumn } from '@/components/common/crud/operator/operator.column'

const ITable = ({
  // 表格列数据：格式为{ title: '', dataIndex: '', render: func, others: {} }，数组
  // 最后一列操作列，默认只有编辑和删除操作，需要添加其它的操作，可以调用方法
  columns,
  // 表格loading属性
  tableLoading,
  // 分页数据，包括分页信息和结果业务数据数组，格式为：{ pagination: {}, data: [] }
  pageResult,
  // 操作列，传入此参数不再展示默认的操作列，后面的参数可以不再需要
  operatorColumn,
  // 操作列需要的方法
  // 显示修改的弹出框
  showUModal,
  // 删除方法
  del,
  // 删除成功的回调方法，一般为获取全部业务数据的方法
  delSuccCallback,
  // 其它操作
  otherOperator
}) => {

  const { Column } = Table
  // console.log('ITable', pageResult)

  return (
    <div style={{ marginTop: '30px' }}>
      {
        pageResult && pageResult.data
        ?
        <Table dataSource={pageResult.data} bordered rowKey="id" loading={tableLoading} pagination={{...pageResult.pagination}} size="middle">
          {getColumns()}
          {operatorColumn || getOperatorColumn(showUModal, del, delSuccCallback, otherOperator)}
        </Table>
        :
        <Empty image={Empty.PRESENTED_IMAGE_DEFAULT} />
      }
    </div>
  )

  function getColumns() {
    if (!columns || columns.length < 1) return null
    return columns.map(column =>
      <Column title={column.title} dataIndex={column.dataIndex} key={column.dataIndex} render={column.render} {...column.others} />
    )
  }
}

export default memo(ITable)
