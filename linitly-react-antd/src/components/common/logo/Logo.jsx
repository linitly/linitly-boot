import './Logo.less'

const Logo = ({
  collapsed
}) => {

  return (
    <div className="logo" >
      <a href="/">
        {
          collapsed ?
          <img src="/logo.svg" alt="logo" style={{ margin: '0 auto', float: 'none' }} />
          :
          <>
            <img src="/logo.svg" alt="logo"/>
            <h1>Linitly React Antd</h1>
          </>
        }
      </a>
    </div>
  )
}

export default Logo
