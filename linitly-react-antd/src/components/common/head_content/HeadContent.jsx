import { useState } from 'react'
import { Popover, Avatar, Modal, Dropdown, Menu } from 'antd';
import {
  UserOutlined,
  LogoutOutlined,
  ExclamationCircleOutlined,
  SettingOutlined
} from '@ant-design/icons';

import './HeadContent.less'

import ChangePassword from '@/pages/user/ChangePassword'

const HeadContent = ({
  avatar,
  username,
  logout
}) => {

  const initModalProperties = {visible: false, cancel: null}
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const [ modalProperties, setModalProperties ] = useState(initModalProperties)

  const menu = (
    <Menu onClick={menuClickHandle}>
      <Menu.Item key={1}>
        <a onClick={e => e.preventDefault()}>
          <SettingOutlined />&nbsp;&nbsp;修改密码
        </a>
      </Menu.Item>
    </Menu>
  )

  return (
    <div className="head-content">
      <Dropdown overlay={menu}>
        <div className="user-info">
          <span className="avatar">
            {
              avatar ? <Avatar size="small" src={avatar} /> : <Avatar size="small" icon={<UserOutlined />} />
            }
          </span>
          <span className="username">欢迎您，{username}</span>
        </div>
      </Dropdown>
      <Popover placement="bottom" content="退出" trigger="hover">
        <div className="logout"  onClick={logoutConfirm}>
          <LogoutOutlined></LogoutOutlined>
        </div>
      </Popover>
      <ChangePassword {...modalProperties} />
    </div>
  )

  function logoutConfirm() {
    Modal.confirm({
      title: '确定要退出登录吗？',
      icon: <ExclamationCircleOutlined />,
      okText: '确认',
      cancelText: '取消',
      onOk: logout
    });
  }

  function menuClickHandle({key}) {
    switch (key) {
      case '1':
        showChangePasswordForm()
        break;
      default:
        break;
    }
  }

  function showChangePasswordForm() {
    setModalProperties({
      visible: true,
      cancel: () => setModalProperties(initModalProperties)
    })
  }
}

export default HeadContent
