import { Breadcrumb } from 'antd';

const IBreadcrumb = ({
  data
}) => {

  return (
    <Breadcrumb>
      {
        data.map((value, index) => <Breadcrumb.Item key={index}>{value}</Breadcrumb.Item>)
      }
    </Breadcrumb>
  )
}

export default IBreadcrumb
