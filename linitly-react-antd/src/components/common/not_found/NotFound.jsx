import { Button, Result } from "antd";
import React from "react";
import "./notFound.less";

const NotFound = () => {
  return (
    <div className="not-found">
      <Result
        status="404"
        title="404"
        subTitle="您访问的页面不存在"
        extra={<Button type="primary">回到首页</Button>}
      />
    </div>
  );
};

export default NotFound;
