import { memo, useState } from 'react'
import { Popconfirm } from 'antd'

import { successPrompt } from '@/utils/default.prompt.js'

const DeletePop = ({
  del,
  successCallback,
  children
}) => {

  const [ visible, setVisible ] = useState(false)
  const [ confirmLoading, setConfirmLoading ] = useState(false)

  return (
    <Popconfirm title="确定要删除吗？" onConfirm={confirm} visible={visible} onCancel={cancel} okButtonProps={{ loading: confirmLoading }}
    onClick={showPop}>
      {children}
    </Popconfirm>
  )

  function confirm() {
    setConfirmLoading(true)
    del().then(() => {
      setVisible(false);
      setConfirmLoading(false);
      successPrompt('删除成功')
      successCallback()
    }).catch(() => {
      setVisible(false);
      setConfirmLoading(false);
    })
  }

  function cancel() {
    setVisible(false);
  }

  function showPop() {
    setVisible(true);
  }
}

export default memo(DeletePop)
