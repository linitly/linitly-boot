import { memo, useEffect, useState } from "react"

const Image = ({
  style = {},
  src,
  className = '',
  defaultImage,
  onClick,
}) => {

  const [ imageSrc, setImageSrc ] = useState('')
  useEffect(() => {
    setImageSrc(src || defaultImage)
  }, [src, defaultImage])

  return (
    <img className={className} style={style} src={imageSrc} onError={imageErr} onClick={onClick} alt="图片加载失败" />
  )

  function imageErr() {
    setImageSrc(defaultImage)
  }
}

export default memo(Image)
