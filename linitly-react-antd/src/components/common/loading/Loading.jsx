import { Spin } from 'antd';
import './Loading.less'

const Loading = () => {
  return (
    <div className="loading">
      <Spin tip="页面加载中..." size="large">
      </Spin>
    </div>
  )
}

export default Loading
