import { SET_USER } from '../constant'

const initUser = {}
export default function user(preState = initUser, action) {
  switch (action.type) {
    case SET_USER:
      return action.data
    default:
      return preState
  }
}
