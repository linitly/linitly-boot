import { SET_USER } from '../constant'

export const createSetUserAction = data => ({type: SET_USER, data})
