package org.linitly.boot;

import com.alibaba.fastjson.JSON;
import com.mongodb.client.result.UpdateResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.linitly.boot.base.entity.SysQuartzJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class MongoApiTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void insert() {
        SysQuartzJob job = new SysQuartzJob().setJobClassName("test-jobClassName").setJobName("test-jobName")
                .setCronExpression("test-corn").setDescription("test-desc");
        job.setId(1L);
        mongoTemplate.save(job);
    }

    @Test
    public void update() {
        Query query = new Query(Criteria.where("id").is(1L));
        Update update = new Update().set("jobName", "test-jobName-2");
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, SysQuartzJob.class);
        System.out.println(updateResult.getMatchedCount());
    }

    @Test
    public void findById() {
        Query query = new Query(Criteria.where("id").is(1L));
        SysQuartzJob job = mongoTemplate.findOne(query, SysQuartzJob.class);
        System.out.println(JSON.toJSONString(job));
    }

    @Test
    public void delete() {
        Query query = new Query(Criteria.where("id").is(1L));
        mongoTemplate.remove(query, SysQuartzJob.class);
    }
}
