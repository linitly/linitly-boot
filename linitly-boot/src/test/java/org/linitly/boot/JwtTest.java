package org.linitly.boot;

import org.linitly.boot.base.utils.jwt.AbstractJwtUtil;
import org.linitly.boot.base.utils.jwt.JwtUtilFactory;

/**
 * @author: linitly
 * @date: 2020/11/22 22:15
 * @descrption:
 */
public class JwtTest {

    public static void main(String[] args) {
        AbstractJwtUtil jwtUtil = JwtUtilFactory.getJwtUtil(101);
        String token = jwtUtil.generateToken("1");
        String refreshToken = jwtUtil.generateRefreshToken("1");
        System.out.println(token);
        System.out.println(refreshToken);
    }
}
