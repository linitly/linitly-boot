package org.linitly.boot;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.linitly.boot.base.config.SwaggerInfoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class Swagger2 {

    @Autowired
    private SwaggerInfoConfig swaggerInfo;

    private final OpenApiExtensionResolver openApiExtensionResolver;

    @Autowired
    public Swagger2(OpenApiExtensionResolver openApiExtensionResolver) {
        this.openApiExtensionResolver = openApiExtensionResolver;
    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //扫描包路径
                .apis(RequestHandlerSelectors.basePackage(Swagger2.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .extensions(openApiExtensionResolver.buildExtensions(""));
    }

    //构建 api文档的详细信息函数,注意这里的注解引用的是哪个
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title(swaggerInfo.getTitle())
                //描述
                .description(swaggerInfo.getDescription())
                //创建人
                .contact(new Contact(swaggerInfo.getAuthor(), null, null))
                //版本号
                .version(swaggerInfo.getVersion())
                .build();
    }
}
