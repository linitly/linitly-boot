package org.linitly.boot.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.linitly.boot.base.entity.admin.SysDept;

import java.util.List;

/**
 * @author: linitly
 * @date: 2021/10/12 16:06
 * @descrption:
 */
@Data
@ApiModel(value = "部门返回VO")
public class SysDeptVO extends SysDept {

    @ApiModelProperty(value = "子部门信息")
    private List<SysDeptVO> children;
}
