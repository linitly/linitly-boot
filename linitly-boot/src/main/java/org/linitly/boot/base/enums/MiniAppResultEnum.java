package org.linitly.boot.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: linitly
 * @date: 2021/4/25 10:04
 * @descrption:
 */
@Getter
@AllArgsConstructor
public enum MiniAppResultEnum {

    SUCCESS(0, -1, "请求成功"),
    SYSTEM_ERROR(-1, 4101, "小程序系统繁忙"),
    CODE_INVALID(40029, 4102, "小程序code无效"),
    FREQUENCY_LIMIT(45011, 4103, "小程序频率限制，请求过于频繁"),
    APP_SECRET_ERROR(40001, 4104, "app secret错误"),
    GRANT_TYPE_ERROR(40002, 4105, "grant_type错误"),
    APP_ID_ERROR(40013, 4106, "appid错误"),
    OTHER(-1000, 4110, "小程序API出现未知错误"),
    ;

    private Integer miniAppCode;

    private Integer code;

    private String message;
    private static final Map<Integer, MiniAppResultEnum> RESULT_ENUM_MAP = toResultMap();

    public static MiniAppResultEnum getResultEnum(Integer miniAppCode) {
        MiniAppResultEnum miniAppResultEnum = RESULT_ENUM_MAP.get(miniAppCode);
        return miniAppResultEnum == null ? OTHER : miniAppResultEnum;
    }

    private static Map<Integer, MiniAppResultEnum> toResultMap() {
        MiniAppResultEnum[] values = MiniAppResultEnum.values();
        Map<Integer, MiniAppResultEnum> resultEnumMap = new HashMap<>();
        for (int i = 0; i < values.length; i++) {
            resultEnumMap.put(values[i].getMiniAppCode(), values[i]);
        }
        return resultEnumMap;
    }
}
