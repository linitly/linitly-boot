package org.linitly.boot.base.constant.miniapp;

import org.linitly.boot.base.constant.global.GlobalConstant;

/**
 * @author linitly
 * @date 16:22 2021/4/25
 * @description
 */
public interface MiniAppCommonConstant {

    /**
     * 小程序路由前缀
     */
    String URL_PREFIX = "/mini-app";

    /**
     * redis存储后台key中缀
     */
    String REDIS_KEY_INFIX = "mini_app:";

    /**
     * 返回给小程序用户的token名称
     */
    String MINI_APP_TOKEN = "token";

    /**
     * 小程序token过期时间(秒)
     */
    long MINI_APP_TOKEN_EXPIRE_SECOND = 24 * 60 * 60;

    /**
     * 小程序token存入redis的前缀
     */
    String MINI_APP_TOKEN_PREFIX = GlobalConstant.REDIS_KEY_PREFIX + REDIS_KEY_INFIX + "token:";

    /**
     * 小程序最后一次过期token存入redis的前缀
     */
    String MINI_APP_LAST_EXPIRED_TOKEN_PREFIX = GlobalConstant.REDIS_KEY_PREFIX + REDIS_KEY_INFIX + "expire_token:";
}
