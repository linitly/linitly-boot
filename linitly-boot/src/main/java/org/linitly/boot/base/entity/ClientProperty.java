package org.linitly.boot.base.entity;

import lombok.Data;
import org.linitly.boot.base.utils.auth.strategy.TokenModuleStrategy;

/**
 * @author: linitly
 * @date: 2021/5/24 15:35
 * @descrption: jwt相关属性实体
 */
@Data
public class ClientProperty {

    private TokenModuleStrategy tokenModuleStrategy;

    private String urlPrefix;
}
