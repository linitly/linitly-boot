package org.linitly.boot.base.entity.miniapp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: linitly
 * @date: 2021/4/25 14:52
 * @descrption:
 */
@Data
@ApiModel(value = "小程序获取用户信息的请求实体")
public class MiniAppUserInfoReqEntity {

    @NotBlank(message = "rawData不能为空")
    @ApiModelProperty(value = "不包括敏感信息的原始数据字符串", required = true)
    private String rawData;

    @NotBlank(message = "signature不能为空")
    @ApiModelProperty(value = "使用sha1(rawData+sessionkey)得到的字符串", required = true)
    private String signature;

    @NotBlank(message = "encryptedData不能为空")
    @ApiModelProperty(value = "包括敏感数据在内的完整用户信息的加密数据", required = true)
    private String encryptedData;

    @NotBlank(message = "iv不能为空")
    @ApiModelProperty(value = "加密算法的初始向量", required = true)
    private String iv;
}
