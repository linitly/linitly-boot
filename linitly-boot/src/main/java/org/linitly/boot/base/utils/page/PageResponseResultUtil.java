package org.linitly.boot.base.utils.page;

import org.linitly.boot.base.helper.entity.PageEntity;
import org.linitly.boot.base.helper.entity.PageResponseResult;

import java.util.List;

/**
 * @author: linitly
 * @date: 2020/5/6 15:56
 * @descrption: 分页结果工具类(适用于集合数据量小的方式):数据库一对多查询时，会造成分页信息错误
 */
public class PageResponseResultUtil {

    public static <T> PageResponseResult getPageResponseResult(List<T> list, int pageNumber, int pageSize) {
        PageEntity pageEntity = PageEntityUtil.getPageEntity(list, pageNumber, pageSize);
        pageNumber = pageNumber * pageSize > pageEntity.getTotalCount() ? pageEntity.getTotalPages() : pageNumber;
        list = list.subList((pageNumber - 1) * pageSize, pageNumber * pageSize > pageEntity.getTotalCount() ? list.size() : pageNumber * pageSize);
        return new PageResponseResult<>(pageEntity, list);
    }
}
