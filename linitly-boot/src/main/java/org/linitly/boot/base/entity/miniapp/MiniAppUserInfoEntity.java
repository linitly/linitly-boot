package org.linitly.boot.base.entity.miniapp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: linitly
 * @date: 2021/4/25 15:08
 * @descrption:
 */
@Data
@ApiModel(value = "微信用户信息实体")
public class MiniAppUserInfoEntity {

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户性别:0:未知;1:男性;2:女性;")
    private Integer gender;

    @ApiModelProperty(value = "用户所在国家")
    private String country;

    @ApiModelProperty(value = "用户所在省份")
    private String province;

    @ApiModelProperty(value = "用户所在城市")
    private String city;

    @ApiModelProperty(value = "用户头像图片的URL")
    private String avatarUrl;

    @ApiModelProperty(value = "显示country，province，city所用的语言")
    private String language;

    private MiniAppPhoneEntity phoneEntity;
}
