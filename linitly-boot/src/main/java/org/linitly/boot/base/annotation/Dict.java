package org.linitly.boot.base.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.linitly.boot.base.handle.DictSerialize;

import java.lang.annotation.*;

/**
 * @author linitly
 * @date 14:28 2020/12/4
 * @description 字典注解
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = DictSerialize.class)
public @interface Dict {

    /**
     * 字典名，默认为该属性的驼峰转换后的属性名
     */
    String code() default "";

    /**
     * 返回属性名（指定返回的字段键值）
     * 为空则默认使用属性名+Dict，例如【sexDict】
     */
    String dictText() default "";

    /**
     * 返回属性名的后缀
     * dictText为空时才生效，例如原属性名为【sex】，生成新属性名为【sexDict】
     */
    String dictTextSuffix() default "Dict";
}
