package org.linitly.boot.base.utils.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.constant.admin.AdminJwtConstant;
import org.linitly.boot.base.constant.global.JwtConstant;
import org.linitly.boot.base.enums.SystemEnum;
import org.linitly.boot.base.enums.TokenModuleEnum;
import org.linitly.boot.base.utils.auth.AuthFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class JwtAdminUtil extends AbstractJwtUtil {

    private static JwtAdminUtil jwtAdminUtil;

    private JwtAdminUtil() {
        super(AdminJwtConstant.ADMIN_ALGORITHM, AdminJwtConstant.JWT_SALT, AuthFactory.getAuth(SystemEnum.ADMIN.getSystemCode()), TokenModuleEnum.DOUBLE_TOKEN);
    }

    public static JwtAdminUtil getInstance() {
        if (jwtAdminUtil == null) {
            synchronized (JwtAdminUtil.class) {
                if (jwtAdminUtil == null) {
                    jwtAdminUtil = new JwtAdminUtil();
                }
            }
        }
        return jwtAdminUtil;
    }

    @Override
    public String generateJwt(Map<String, Object> claims, String subject, long expireSecond) {
        claims = claims == null ? new HashMap<>() : claims;
        claims.put(JwtConstant.UUID_JWT_KEY, UUID.randomUUID().toString());
        return Jwts.builder()
                .setSubject(subject)
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + expireSecond * 1000))
                .signWith(algorithm, getKeyInstance())
                .compact();
    }

    @Override
    public String generateToken(String userId, String... message) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(AdminJwtConstant.ADMIN_USER_ID, userId);
        return generateJwt(claims, userId, AdminCommonConstant.ADMIN_TOKEN_EXPIRE_SECOND);
    }

    @Override
    public String generateRefreshToken(String userId, String... message) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(AdminJwtConstant.ADMIN_USER_ID, userId);
        return generateJwt(claims, userId, AdminCommonConstant.ADMIN_REFRESH_TOKEN_EXPIRE_SECOND);
    }

    @Override
    public String getToken(HttpServletRequest request) {
        return request == null ? null : request.getHeader(AdminCommonConstant.ADMIN_TOKEN);
    }

    @Override
    public String getRefreshToken(HttpServletRequest request) {
        return request == null ? null : request.getHeader(AdminCommonConstant.ADMIN_REFRESH_TOKEN);
    }

    @Override
    public void setTokensHead(String token, String refreshToken) {
        getTokenModuleStrategy().setTokensHead(AdminCommonConstant.ADMIN_TOKEN, AdminCommonConstant.ADMIN_REFRESH_TOKEN, token, refreshToken);
    }

    @Override
    public String getUserId(HttpServletRequest request) {
        String token = getToken(request);
        Map<String, Object> claims = null;
        try {
            claims = parseJwt(token);
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        }
        return claims.get(AdminJwtConstant.ADMIN_USER_ID).toString();
    }

    @Override
    public void interceptorValid(HttpServletRequest request) {
        getTokenModuleStrategy().interceptorValid(AdminCommonConstant.ADMIN_TOKEN);
    }

    @Override
    public void validToken(String token) {
        getTokenModuleStrategy().validToken(token);
    }

    @Override
    public void validExpiredToken(String token) {
        getTokenModuleStrategy().validExpiredToken(token);
    }

    @Override
    public void validRefreshToken(String refreshToken) {
        getTokenModuleStrategy().validRefreshToken(refreshToken);
    }

    @Override
    public void generateNewToken() {
        getTokenModuleStrategy().generateNewToken(AdminCommonConstant.ADMIN_TOKEN);
    }
}
