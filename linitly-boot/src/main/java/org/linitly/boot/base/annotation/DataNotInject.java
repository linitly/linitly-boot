package org.linitly.boot.base.annotation;

import java.lang.annotation.*;

/**
 * @author linitly
 * @date 2019/8/27 10:57
 * @description 添加或修改时，不修改SQL插入创建人、修改人、修改时间等信息的注解
 */
@Documented
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataNotInject {
}
