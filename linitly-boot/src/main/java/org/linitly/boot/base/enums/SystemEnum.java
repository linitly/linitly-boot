package org.linitly.boot.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: linitly
 * @date: 2020/11/22 20:24
 * @descrption:
 */
@Getter
@AllArgsConstructor
public enum SystemEnum {

    ADMIN(101),
    MINI_APP(201),
    ;

    private Integer systemCode;

    private static final Map<Integer, SystemEnum> ENUM_MAP = toResultMap();

    public static SystemEnum getEnum(Integer systemCode) {
        return ENUM_MAP.get(systemCode);
    }

    private static Map<Integer, SystemEnum> toResultMap() {
        SystemEnum[] values = SystemEnum.values();
        Map<Integer, SystemEnum> enumMap = new HashMap<>();
        for (int i = 0; i < values.length; i++) {
            enumMap.put(values[i].getSystemCode(), values[i]);
        }
        return enumMap;
    }
}
