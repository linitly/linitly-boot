package org.linitly.boot.base.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.linitly.boot.base.annotation.RequirePermission;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.dto.SysDeptDTO;
import org.linitly.boot.base.entity.admin.SysDept;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.boot.base.service.SysDeptService;
import org.linitly.boot.base.vo.SysDeptVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: linitly-generator
 * @date: 2020-11-24 10:41
 * @description: 
 */
@Result
@RestController
@RequestMapping(AdminCommonConstant.URL_PREFIX + "/sysDept")
@Api(tags = "系统部门管理")
public class SysDeptController {

    @Autowired
    private SysDeptService sysDeptService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加系统部门")
    @RequirePermission("dept:add")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) SysDeptDTO dto, BindingResult bindingResult) {
        sysDeptService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改系统部门")
    @RequirePermission("dept:update")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) SysDeptDTO dto, BindingResult bindingResult) {
        sysDeptService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询系统部门")
    public SysDept findById(@PathVariable Long id) {
        return sysDeptService.findById(id);
    }

    @PostMapping("/findAll")
    @ApiOperation(value = "根据条件查询系统部门列表")
    @RequirePermission("dept:search")
    public List<SysDeptVO> findAll(@RequestBody(required = false) SysDept sysDept) {
        return sysDeptService.findAll(sysDept);
    }

    @PostMapping("/findAllSelect")
    @ApiOperation(value = "查询系统部门列表用于选择")
    public List<SysDeptVO> findAllSelect() {
        return sysDeptService.findAll(null);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除系统部门")
    @RequirePermission("dept:delete")
    public void deleteById(@PathVariable Long id) {
        sysDeptService.deleteById(id);
    }
}