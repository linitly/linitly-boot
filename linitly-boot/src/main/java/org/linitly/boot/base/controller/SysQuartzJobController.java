package org.linitly.boot.base.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.RequirePermission;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.dto.SysQuartzJobInsertAndUpdateDTO;
import org.linitly.boot.base.dto.SysQuartzJobSearchDTO;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.entity.SysQuartzJob;
import org.linitly.boot.base.exception.CommonException;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.boot.base.service.SysQuartzJobService;
import org.quartz.CronExpression;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.util.List;

@Result
@RestController
@RequestMapping(AdminCommonConstant.URL_PREFIX + "/job")
@Api(tags = "定时任务管理")
public class SysQuartzJobController {

    @Autowired
    private SysQuartzJobService jobService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加定时任务")
    @RequirePermission("job:add")
    public void add(@RequestBody @Validated({InsertValidGroup.class}) SysQuartzJobInsertAndUpdateDTO dto, BindingResult bindingResult) {
        validJobMsg(dto);
        jobService.insertJob(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改定时任务")
    @RequirePermission("job:update")
    public void update(@RequestBody @Validated({Default.class, UpdateValidGroup.class}) SysQuartzJobInsertAndUpdateDTO dto,
                                 BindingResult bindingResult) {
        validJobMsg(dto);
        jobService.updateJob(dto);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询定时任务列表")
    @RequirePermission("job:update")
    public List<SysQuartzJob> list(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber,
                                   @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize,
                                   @RequestBody(required = false) SysQuartzJobSearchDTO dto) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return jobService.findQuartzJobs(dto);
    }

    @PostMapping("/pause_or_resume/{id}")
    @ApiOperation(value = "启动或暂停一个任务")
    @RequirePermission("job:start/pause")
    public void pauseOrResume(@PathVariable Long id) {
        jobService.pauseOrResumeJob(id);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询任务信息")
    public SysQuartzJob findById(@PathVariable Long id) {
        return jobService.findById(id);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "删除任务")
    @RequirePermission("job:delete")
    public void delete(@PathVariable Long id) {
        jobService.deleteById(id);
    }

    private void validJobMsg(SysQuartzJobInsertAndUpdateDTO dto) {
        if (!CronExpression.isValidExpression(dto.getCronExpression())) {
            throw new CommonException("cron表达式错误");
        }
        try {
            // 判断类是否存在
            Class<?> jobClass = Class.forName(dto.getJobClassName());
            // 判断是否是任务类
            boolean isJobClass = QuartzJobBean.class.isAssignableFrom(jobClass) || Job.class.isAssignableFrom(jobClass);
            if (!isJobClass) {
                throw new CommonException("任务类名错误");
            }
        } catch (ClassNotFoundException e) {
            throw new CommonException("任务执行类名不正确，找不到对应类");
        }
    }
}
