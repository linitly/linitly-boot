package org.linitly.boot.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author linitly
 * @date 13:27 2021/5/21
 * @description
 */
@Getter
@AllArgsConstructor
public enum TokenModuleEnum {

    SINGLE_TOKEN_EXPIRE("单token模式", "单token模式，会自动过期"),
    SINGLE_TOKEN_RENEW("单token模式", "单token模式，可自动续期"),
    DOUBLE_TOKEN("双token模式", "双token模式，可自动续期"),
    ;

    private final String name;
    private final String desc;
}
