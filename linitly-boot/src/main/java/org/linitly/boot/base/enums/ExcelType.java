package org.linitly.boot.base.enums;

/**
 * @author: linitly
 * @date: 2020/5/9 10:56
 * @descrption: excel类型枚举
 */
public enum ExcelType {

    XLS,
    XLSX,
    ;
}
