package org.linitly.boot.base.vo;

import lombok.Data;

/**
 * @author: linitly
 * @date: 2020/12/6 17:25
 * @descrption:
 */
@Data
public class SysDataDictItemCacheVO {

    private String code;

    private String value;

    private String text;
}
