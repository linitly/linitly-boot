package org.linitly.boot.base.utils;

import org.linitly.boot.base.constant.global.GlobalConstant;
import org.linitly.boot.base.helper.entity.BaseEntity;
import org.linitly.boot.base.utils.auth.AbstractAuth;
import org.linitly.boot.base.utils.bean.SpringBeanUtil;
import org.linitly.boot.base.utils.jwt.AbstractJwtUtil;
import org.linitly.boot.base.utils.jwt.JwtUtilFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @author: linitly
 * @date: 2020/12/9 8:51
 * @descrption:
 */
public class LinitlyUtil {

    public static void setJwt() {
        HttpServletRequest request = SpringBeanUtil.getRequest();
        setJwt(request);
    }

    public static void setJwt(HttpServletRequest request) {
        request.setAttribute(GlobalConstant.JWT_ATTRIBUTE, getJwt(request));
    }

    public static AbstractJwtUtil getJwt() {
        return (AbstractJwtUtil) SpringBeanUtil.getRequest().getAttribute(GlobalConstant.JWT_ATTRIBUTE);
    }

    private static AbstractJwtUtil getJwt(HttpServletRequest request) {
        return JwtUtilFactory.getJwtUtil(request);
    }

    public static AbstractAuth getAuth() {
        return getJwt().getAbstractAuth();
    }

    public static String generateToken(String userId, String... message) {
        return getJwt().generateToken(userId, message);
    }

    public static String generateRefreshToken(String userId, String... message) {
        return getJwt().generateRefreshToken(userId, message);
    }

    public static String getRequestToken() {
        return getJwt().getToken(SpringBeanUtil.getRequest());
    }

    public static String getRequestRefreshToken() {
        return getJwt().getRefreshToken(SpringBeanUtil.getRequest());
    }

    public static String getCurrentUserId() {
        return getJwt().getUserId(SpringBeanUtil.getRequest());
    }

    public static void interceptorValid() {
        getJwt().interceptorValid(SpringBeanUtil.getRequest());
    }

    public static String getCacheToken(String userId) {
        return getAuth().getToken(userId);
    }

    public static String getCurrentCacheToken() {
        return getCacheToken(getCurrentUserId());
    }

    public static String getCacheRefreshToken(String userId) {
        return getAuth().getRefreshToken(userId);
    }

    public static String getCurrentCacheRefreshToken() {
        return getCacheRefreshToken(getCurrentUserId());
    }

    public static Set<?> getCacheRoles(String userId) {
        return getAuth().getRoles(userId);
    }

    public static Set<?> getCurrentCacheRoles() {
        return getCacheRoles(getCurrentUserId());
    }

    public static Set<?> getCacheDepts(String userId) {
        return getAuth().getDepts(userId);
    }

    public static Set<?> getCurrentCacheDepts() {
        return getCacheDepts(getCurrentUserId());
    }

    public static Set<?> getCachePosts(String userId) {
        return getAuth().getPosts(userId);
    }

    public static Set<?> getCurrentCachePosts() {
        return getCachePosts(getCurrentUserId());
    }

    public static Set<?> getCacheFunctionPermissions(String userId) {
        return getAuth().getFunctionPermissions(userId);
    }

    public static Set<?> getCurrentCacheFunctionPermissions() {
        return getCacheFunctionPermissions(getCurrentUserId());
    }

    public static void loginCacheSet(String userId, String token) {
        loginCacheSet(userId, token, null, null, null, null, null);
    }

    public static void loginCacheSet(String userId, String token, String refreshToken) {
        loginCacheSet(userId, token, refreshToken, null, null, null, null);
    }

    public static void loginCacheSet(String userId, String token, String refreshToken, Set<?> deptIds, Set<?> postIds, Set<?> roles, Set<?> functionPermissions) {
        getAuth().loginRedisSet(userId, token, refreshToken, deptIds, postIds, roles, functionPermissions);
        setTokensHeader(token, refreshToken);
    }

    private static void setTokensHeader(String token, String refreshToken) {
        getJwt().setTokensHead(token, refreshToken);
    }

    public static void logoutCacheDel(String userId) {
        getAuth().logoutRedisDel(userId);
    }

    public static void logoutCurrentCacheDel() {
        logoutCacheDel(getCurrentUserId());
    }

    public static void updateCacheRoles(String userId, Set<?> roles) {
        getAuth().updateRoles(userId, roles);
    }

    public static void updateCacheDepts(String userId, Set<?> deptIds) {
        getAuth().updateDepts(userId, deptIds);
    }

    public static void updateCachePosts(String userId, Set<?> postIds) {
        getAuth().updatePosts(userId, postIds);
    }

    public static void updateCacheFunctionPermissions(String userId, Set<?> functionPermissions) {
        getAuth().updateFunctionPermissions(userId, functionPermissions);
    }
}
