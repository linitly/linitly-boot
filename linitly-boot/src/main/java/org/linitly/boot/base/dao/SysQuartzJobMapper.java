package org.linitly.boot.base.dao;

import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.boot.base.annotation.LogIgnore;
import org.linitly.boot.base.dto.SysQuartzJobSearchDTO;
import org.linitly.boot.base.entity.SysQuartzJob;

import java.util.List;

public interface SysQuartzJobMapper {

    SysQuartzJob findByJobNameAndId(String jobName, Long id);

    SysQuartzJob findById(Long id);

    List<SysQuartzJob> findJobs(SysQuartzJobSearchDTO dto);

    void insert(SysQuartzJob job);

    @LogIgnore
    void updateStatusById(SysQuartzJob job);

    void updateById(SysQuartzJob job);

    @DeleteBackup
    void deleteById(Long id);
}
