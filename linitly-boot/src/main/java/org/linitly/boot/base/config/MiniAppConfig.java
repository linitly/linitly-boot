package org.linitly.boot.base.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: linitly
 * @date: 2021/4/25 9:58
 * @descrption:
 */
@Data
@Component
@ConfigurationProperties(prefix = "mini-app")
public class MiniAppConfig {

    private String appId;

    private String appSecret;

    private String codeToSessionUrl;

    private String accessTokenUrl;
}
