package org.linitly.boot.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DBCommandTypeEnum {

    INSERT("INSERT", "新增"),
    UPDATE("UPDATE", "修改"),
    DELETE("DELETE", "删除"),
    SELECT("SELECT", "查询"),
    ;

    private final String commandType;

    private final String operator;
}
