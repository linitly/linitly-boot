package org.linitly.boot.base.entity.miniapp;

import lombok.Data;

/**
 * @author: linitly
 * @date: 2021/4/25 10:02
 * @descrption: 微信登陆返回实体
 */
@Data
public class MiniAppLoginResEntity {

    private String openid;

    private String sessionKey;

    private String unionid;

    private Integer errcode;

    private String errmsg;
}
