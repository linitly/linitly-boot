package org.linitly.boot.base.utils.jwt;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Map;

/**
 * @author: linitly
 * @date: 2020/11/22 16:55
 * @descrption:
 */
interface JwtUtil {

    Key getKeyInstance();

    Map<String, Object> parseJwt(String jwt);

    String generateJwt(Map<String, Object> claims, String subject, long expireSecond);

    String generateToken(String userId, String... message);

    String generateRefreshToken(String userId, String... message);

    String getToken(HttpServletRequest request);

    String getRefreshToken(HttpServletRequest request);

    void setTokensHead(String token, String refreshToken);

    String getUserId(HttpServletRequest request);

    void interceptorValid(HttpServletRequest request);

    Map parseToken(String token);

    Map parseRefreshToken(String refreshToken);

    void validToken(String token);

    void validRefreshToken(String refreshToken);

    void validExpiredToken(String token);

    void generateNewToken();
}