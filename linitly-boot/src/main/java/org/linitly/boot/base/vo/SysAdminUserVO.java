package org.linitly.boot.base.vo;

import lombok.Data;
import org.linitly.boot.base.entity.admin.SysAdminUser;

import java.util.List;

/**
 * @author: linitly
 * @date: 2021/10/8 14:31
 * @descrption:
 */
@Data
public class SysAdminUserVO extends SysAdminUser {

    private List<AdminUserRoleVO> roles;

    private List<AdminUserPostVO> posts;

    @Data
    static class AdminUserRoleVO {
        private Long id;

        private String name;
    }

    @Data
    static class AdminUserPostVO {
        private Long id;

        private String name;
    }
}
