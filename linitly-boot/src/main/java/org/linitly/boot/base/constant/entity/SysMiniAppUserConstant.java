package org.linitly.boot.base.constant.entity;

/**
 * @author: linitly-generator
 * @date: 2020-11-25 10:26
 * @description: 
 */
public interface SysMiniAppUserConstant {

    /**
     * 小程序用户token存入redis时对于用户id加密使用盐
     */
    String TOKEN_ID_SALT = "UWadsaOQMFSdsaffa……*……&@@($)!)(@#KJSLKASJRdgdfgW(E*QW)(RUOIgfhkdladsuqioebdsahnklcsajklfWJFKSAHFOIYR*O&YWQIPOHRFIOAESYI";
}