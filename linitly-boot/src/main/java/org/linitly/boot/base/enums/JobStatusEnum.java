package org.linitly.boot.base.enums;

/**
 * @author: linitly
 * @date: 2020/5/27 9:45
 * @descrption:
 */
public enum JobStatusEnum {

    RUNNING(1),     // 运行中
    PAUSED(2),      // 已暂停
    ;

    private final Integer status;

    JobStatusEnum(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return this.status;
    }
}
