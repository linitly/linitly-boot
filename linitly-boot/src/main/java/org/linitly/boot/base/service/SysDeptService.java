package org.linitly.boot.base.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.CollectionUtils;
import org.linitly.boot.base.dao.SysDeptMapper;
import org.linitly.boot.base.dto.SysDeptDTO;
import org.linitly.boot.base.entity.admin.SysDept;
import org.linitly.boot.base.exception.CommonException;
import org.linitly.boot.base.vo.SysDeptVO;
import org.linitly.boot.base.vo.SysMenuTreeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: linitly-generator
 * @date: 2020-11-24 10:41
 * @description: 
 */
@Service
public class SysDeptService {

    private static final Long ROOT_ID = 0L;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Transactional
    public void insert(SysDeptDTO dto) {
        checkExist(dto.getParentId(), dto.getName(), dto.getId());
        SysDept sysDept = new SysDept();
        BeanUtils.copyProperties(dto, sysDept);
        sysDeptMapper.insertSelective(sysDept);
        if (sysDept.getParentId() > 0) {
            sysDeptMapper.updateChildNumberIncrById(sysDept.getParentId());
        }
    }

    @Transactional
    public void updateById(SysDeptDTO dto) {
        checkExist(dto.getParentId(), dto.getName(), dto.getId());
        if (dto.getId().equals(dto.getParentId())) {
            throw new CommonException("父部门不可以指向自己");
        }
        SysDept before = sysDeptMapper.findById(dto.getId());
        if (!before.getParentId().equals(dto.getParentId()) && dto.getParentId() > 0) {
            sysDeptMapper.updateChildNumberIncrById(dto.getParentId());
        }
        if (!before.getParentId().equals(dto.getParentId()) && before.getParentId() > 0) {
            sysDeptMapper.updateChildNumberDecrById(before.getParentId());
        }
        BeanUtils.copyProperties(dto, before);
        sysDeptMapper.updateByIdSelective(before);
    }

    public SysDept findById(Long id) {
        return sysDeptMapper.findById(id);
    }

    public List<SysDeptVO> findAll(SysDept sysDept) {
        List<SysDeptVO> depts = sysDeptMapper.findAll(sysDept);
        if (CollectionUtils.isEmpty(depts)) return depts;

        Multimap<Long, SysDeptVO> deptMultimap = ArrayListMultimap.create();
        ArrayList<SysDeptVO> deptTreeList = Lists.newArrayList();
        List<Long> deptIds = depts.stream().map(SysDeptVO::getId).collect(Collectors.toList());

        for (SysDeptVO dept : depts) {
            deptMultimap.put(dept.getParentId(), dept);
            if (dept.getParentId().equals(ROOT_ID) || !deptIds.contains(dept.getParentId())) {
                deptTreeList.add(dept);
            }
        }
        deptTreeList.sort(deptSeqComparator);
        transformTree(deptTreeList, deptMultimap);
        return deptTreeList;
    }

    private void transformTree(List<SysDeptVO> deptTreeList, Multimap<Long, SysDeptVO> deptMultimap) {
        for (SysDeptVO dept : deptTreeList) {
            if (dept.getChildNumber() < 1) {
                // 没有子部门，直接跳过本次循环即可
                continue;
            }
            List<SysDeptVO> childDepts = (List<SysDeptVO>) deptMultimap.get(dept.getId());
            if (CollectionUtils.isEmpty(childDepts)) continue;
            childDepts.sort(deptSeqComparator);
            dept.setChildren(childDepts);
            transformTree(childDepts, deptMultimap);
        }
    }

    public void deleteById(Long id) {
        if (sysDeptMapper.countByParentId(id) > 0) {
            throw new CommonException("当前部门存在子部门，无法删除");
        }
        sysDeptMapper.deleteById(id);
    }

    private void checkExist(Long parentId, String deptName, Long id) {
        if (sysDeptMapper.countByNameAndParentId(parentId, deptName, id) > 0) {
            throw new CommonException("同一层级下存在相同名字的部门");
        }
    }

    private final Comparator<SysDeptVO> deptSeqComparator = Comparator.comparing(SysDeptVO::getSort);
}