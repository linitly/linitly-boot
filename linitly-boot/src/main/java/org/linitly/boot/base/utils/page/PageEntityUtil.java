package org.linitly.boot.base.utils.page;

import org.linitly.boot.base.helper.entity.PageEntity;

import java.util.List;

/**
 * @author: linitly
 * @date: 2020/12/23 16:37
 * @descrption:
 */
public class PageEntityUtil {

    public static <T> PageEntity getPageEntity(List<T> list, int pageNumber, int pageSize) {
        long totalCount = list == null ? 0 : list.size();
        return getPageEntity(totalCount, pageNumber, pageSize);
    }

    public static PageEntity getPageEntity(long totalCount, int pageNumber, int pageSize) {
        PageEntity pageEntity = new PageEntity();
        if (totalCount == 0) {
            pageEntity.setPageNumber(0);
            pageEntity.setPageSize(pageSize);
            pageEntity.setTotalCount(0);
            return pageEntity;
        }

        long totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

        pageEntity.setPageNumber(pageNumber > totalPage ? (int)totalPage : pageNumber);
        pageEntity.setPageSize(pageSize);
        pageEntity.setTotalCount(totalCount);
        pageEntity.setLastPage(pageNumber >= totalPage);
        pageEntity.setTotalPages((int)totalPage);
        pageEntity.setHasNextPage(totalPage > pageNumber);
        pageEntity.setFirstPage(totalPage == 1);
        pageEntity.setHasPreviousPage(pageNumber > 1 && totalPage > 1);

        return pageEntity;
    }
}
