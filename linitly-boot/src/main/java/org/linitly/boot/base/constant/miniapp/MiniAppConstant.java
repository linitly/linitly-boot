package org.linitly.boot.base.constant.miniapp;

import org.linitly.boot.base.constant.global.GlobalConstant;

public interface MiniAppConstant {

    String ACCESS_TOKEN_REDIS_KEY = GlobalConstant.REDIS_KEY_PREFIX + "mini_app:access_token";

    /** 小程序access_token存储在redis的过期时间，有效期2个小时，我方存储1.95小时 */
    long ACCESS_TOKEN_EXPIRE_TIME = (long) (1.95 * 60 * 60);
}
