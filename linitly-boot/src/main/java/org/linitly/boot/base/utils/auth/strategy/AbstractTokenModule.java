package org.linitly.boot.base.utils.auth.strategy;

import org.linitly.boot.base.utils.auth.AbstractAuth;
import org.linitly.boot.base.utils.bean.SpringBeanUtil;
import org.linitly.boot.base.utils.jwt.AbstractJwtUtil;

/**
 * @author: linitly
 * @date: 2021/5/27 13:53
 * @descrption:
 */
public abstract class AbstractTokenModule implements TokenModuleStrategy {

    protected AbstractJwtUtil jwtUtil;
    protected AbstractAuth auth;

    AbstractTokenModule(AbstractJwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
        this.auth = jwtUtil.getAbstractAuth();
    }

    public String getUserId() {
        return jwtUtil.getUserId(SpringBeanUtil.getRequest());
    }
}
