package org.linitly.boot.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: linitly
 * @date: 2021/10/19 15:20
 * @descrption:
 */
@Getter
@AllArgsConstructor
public enum WSResultType {

    HEART_BRAT(1),
    COMMON(200),


    ;
    private Integer type;

    private static final Map<Integer, WSResultType> ENUM_MAP = toResultMap();

    public static WSResultType getEnum(Integer type) {
        return ENUM_MAP.get(type);
    }

    private static Map<Integer, WSResultType> toResultMap() {
        WSResultType[] values = WSResultType.values();
        Map<Integer, WSResultType> enumMap = new HashMap<>();
        for (WSResultType value : values) {
            enumMap.put(value.getType(), value);
        }
        return enumMap;
    }
}
