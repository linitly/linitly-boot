package org.linitly.boot.base.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.linitly.boot.base.constant.global.MyBatisConstant;
import org.springframework.stereotype.Component;

/**
 * @author Linitly
 * @date 9:41 2022/3/10
 * @description
 */
@Aspect
@Component
public class DataNotInjectAspect {

    @Pointcut("@annotation(org.linitly.boot.base.annotation.DataNotInject)")
    public void dataNotInjectAspect() {
    }

    @Before("dataNotInjectAspect()")
    public void dataNotInjectBefore(JoinPoint joinPoint) {
        MyBatisConstant.MYBATIS_INTERCEPT_PASS.set(true);
    }

    @AfterReturning("dataNotInjectAspect()")
    public void dataNotInjectAfterReturn(JoinPoint joinPoint) {
        MyBatisConstant.MYBATIS_INTERCEPT_PASS.remove();
    }
}
