package org.linitly.boot.base.entity.miniapp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: linitly
 * @date: 2021/4/25 15:16
 * @descrption:
 */
@Data
@ApiModel(value = "小程序手机号实体")
public class MiniAppPhoneEntity {

    @ApiModelProperty(value = "用户绑定的手机号（国外手机号会有区号）")
    private String phoneNumber;

    @ApiModelProperty(value = "没有区号的手机号")
    private String purePhoneNumber;

    @ApiModelProperty(value = "区号")
    private String countryCode;
}
