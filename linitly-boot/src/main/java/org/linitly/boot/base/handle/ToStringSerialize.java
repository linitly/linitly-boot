package org.linitly.boot.base.handle;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author linitly
 * @date 16:45 2021/10/8
 * @description
 */
@Slf4j
public class ToStringSerialize extends JsonSerializer<Object> {

    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        try {
            if (o == null) return;
            jsonGenerator.writeString(o.toString());
        } catch (IOException e) {
            e.printStackTrace();
            log.error("反序列化转字符串出错，字段名为：" + jsonGenerator.getOutputContext().getCurrentName());
        }
    }
}
