package org.linitly.boot.base.entity.miniapp;

import lombok.Data;

/**
 * @author: linitly
 * @date: 2021/4/25 11:22
 * @descrption:
 */
@Data
public class MiniAppAccessTokenResEntity {

    private String accessToken;

    private Integer expiresIn;

    private Integer errcode;

    private String errmsg;
}
