package org.linitly.boot.base.helper.entity;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: linitly
 * @date: 2021/10/19 15:10
 * @descrption:
 */
@Data
@ApiModel(value = "websocket通信使用消息类型")
public class WSResult {

    @ApiModelProperty(value = "消息类型")
    private Integer type;

    @ApiModelProperty(value = "消息体")
    private String message;

    public static WSResult parse(String message) {
        return JSON.parseObject(message, WSResult.class);
    }
}
