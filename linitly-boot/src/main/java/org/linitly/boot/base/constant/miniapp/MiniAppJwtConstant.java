package org.linitly.boot.base.constant.miniapp;

import io.jsonwebtoken.SignatureAlgorithm;

public interface MiniAppJwtConstant {

    /**
     * 小程序token加解密算法
     */
    SignatureAlgorithm MINI_APP_ALGORITHM = SignatureAlgorithm.HS512;

    /**
     * 小程序用户id存储jwt的key值
     */
    String MINI_APP_USER_ID = "mini_app_user_id";

    /**
     * 小程序sessionKey存储jwt的key值
     */
    String SESSION_KEY = "session_key";

    /**
     * 小程序openid存储jwt的key值
     */
    String OPEN_ID = "open_id";

    /**
     * JWT加密盐
     */
    String JWT_SALT = "jwt_salt!!!!@@##miniapphaskdjalk$&*(@!&$(@!JKLAJsad(*&*)(dsadDLKSJKLDSAKLminiapp:-=23891IOP";
}
