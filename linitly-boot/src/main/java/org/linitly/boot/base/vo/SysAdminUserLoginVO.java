package org.linitly.boot.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.entity.admin.SysAdminUser;

import java.util.List;

/**
 * @author: linitly
 * @date: 2020/11/27 15:50
 * @descrption:
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "用户登陆返回VO")
public class SysAdminUserLoginVO extends SysAdminUser {

    @ApiModelProperty(value = "菜单树")
    private List<SysMenuTreeVO> menuTree;
}