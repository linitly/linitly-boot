package org.linitly.boot.base.utils.auth.strategy;

import org.linitly.boot.base.enums.TokenModuleEnum;
import org.linitly.boot.base.exception.CommonException;
import org.linitly.boot.base.utils.jwt.AbstractJwtUtil;

/**
 * @author: linitly
 * @date: 2021/5/24 16:16
 * @descrption:
 */
public class TokenModuleFactory {

    public static AbstractTokenModule getTokenModule(TokenModuleEnum tokenModuleEnum, AbstractJwtUtil jwtUtil) {
        AbstractTokenModule tokenModule = null;
        switch (tokenModuleEnum) {
            case SINGLE_TOKEN_RENEW:
                tokenModule = SingleTokenRenewModule.getInstance(jwtUtil);
                break;
            case SINGLE_TOKEN_EXPIRE:
                tokenModule = SingleTokenExpireModule.getInstance(jwtUtil);
                break;
            case DOUBLE_TOKEN:
                tokenModule = DoubleTokenModule.getInstance(jwtUtil);
                break;
            default:
                throw new CommonException("未知token模式");
        }
        return tokenModule;
    }
}
