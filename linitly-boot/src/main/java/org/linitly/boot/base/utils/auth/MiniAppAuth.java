package org.linitly.boot.base.utils.auth;

import org.linitly.boot.base.constant.entity.SysMiniAppUserConstant;
import org.linitly.boot.base.constant.miniapp.MiniAppCommonConstant;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author linitly
 * @date 16:47 2021/4/25
 * @description 
 */
public class MiniAppAuth extends AbstractAuth {

    private MiniAppAuth() {
        super(SysMiniAppUserConstant.TOKEN_ID_SALT, MiniAppCommonConstant.MINI_APP_TOKEN_PREFIX, null,
                MiniAppCommonConstant.MINI_APP_LAST_EXPIRED_TOKEN_PREFIX, null,
                null, null,
                null, MiniAppCommonConstant.MINI_APP_TOKEN_EXPIRE_SECOND,
                0L, MiniAppCommonConstant.MINI_APP_TOKEN_EXPIRE_SECOND,
                0L, 0L,
                0L, 0L);
    }

    private static final class MiniAppAuthHolder {
        static final MiniAppAuth miniAppAuth = new MiniAppAuth();
    }

    protected static MiniAppAuth getInstance() {
        return MiniAppAuthHolder.miniAppAuth;
    }

    @Override
    public void newTokenRedisSet(String id, String newToken) {
        setRedisToken(id, newToken);
    }

    @Override
    public void loginRedisSet(String id, String token, String refreshToken, Set<?> deptIds, Set<?> postIds,
                              Set<?> roles, Set<?> functionPermissions) {
        setRedisToken(id, token);
    }

    @Override
    public void logoutRedisDel(String id) {
        delRedisToken(id);
    }

    @Override
    public void updateFunctionPermissions(String id, Set<?> functionPermissions) {
    }
}