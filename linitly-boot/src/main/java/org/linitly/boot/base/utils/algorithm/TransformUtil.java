package org.linitly.boot.base.utils.algorithm;

import java.nio.charset.StandardCharsets;

/**
 * @author: linitly
 * @date: 2021/4/23 8:54
 * @descrption: 转换工具类
 */
public class TransformUtil {

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * 字节转换为hex string(16进制字符串)
     */
    public static String byteToHexString(byte src) {
        char[] hexChar = new char[2];
        int v = src & 0xFF;
        hexChar[0] = hexArray[v >>> 4];
        hexChar[1] = hexArray[v & 0x0F];
        return new String(hexChar);
    }

    /**
     * 字节数组转换为hex string(16进制字符串)
     */
    public static String bytesToHexString(byte[] src) {
        char[] hexChars = new char[src.length * 2];
        for (int j = 0; j < src.length; j++) {
            int v = src[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * hex string(16进制字符串)转换为字节数组
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.trim().equals("")) {
            return new byte[0];
        }
        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length() / 2; i++) {
            String subStr = hexString.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }
        return bytes;
    }

    /**
     * hex string(16进制字符串)转换为字符串
     */
    public static String hexStringToString(String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        s = s.replace(" ", "");
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            baKeyword[i] = (byte) (0xff & Integer.parseInt(
                    s.substring(i * 2, i * 2 + 2), 16));
        }
        s = new String(baKeyword, StandardCharsets.UTF_8);
        return s;
    }

    /**
     * int转换为hex string(16进制字符串)，高位补0
     */
    public static String toFullHexString(int num) {
        char[] chs = new char[Integer.SIZE / 4];
        for (int i = 0; i < chs.length; i++) {
            chs[chs.length - 1 - i] = hexArray[(num >> (i * 4)) & 0xf];
        }
        return new String(chs);
    }

    /**
     * 将 int 类型数据转成二进制的字符串，不足 int 类型位数时在前面添“0”以凑足位数
     */
    public static String toFullBinaryString(int num) {
        char[] chs = new char[Integer.SIZE];
        for (int i = 0; i < Integer.SIZE; i++) {
            chs[Integer.SIZE - 1 - i] = (char) (((num >> i) & 1) + '0');
        }
        return new String(chs);
    }

    /**
     * 将 long 类型数据转成二进制的字符串，不足 long 类型位数时在前面添“0”以凑足位数
     */
    public static String toFullBinaryString(long num) {
        char[] chs = new char[Long.SIZE];
        for (int i = 0; i < Long.SIZE; i++) {
            chs[Long.SIZE - 1 - i] = (char) (((num >> i) & 1) + '0');
        }
        return new String(chs);
    }

    /**
     * 将 long 类型数据转成十六进制的字符串，不足 long 类型位数时在前面添“0”以凑足位数
     */
    public static String toFullHexString(long num) {
        char[] chs = new char[Long.SIZE / 4];
        for (int i = 0; i < chs.length; i++) {
            chs[chs.length - 1 - i] = hexArray[(int) ((num >> (i * 4)) & 0xf)];
        }
        return new String(chs);
    }
}
