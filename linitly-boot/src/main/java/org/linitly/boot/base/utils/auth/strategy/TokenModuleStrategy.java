package org.linitly.boot.base.utils.auth.strategy;

/**
 * @author linitly
 * @date 14:10 2021/5/24
 * @description token模式策略接口
 */
public interface TokenModuleStrategy {

    void setTokensHead(String tokenResponseHeadKey, String refreshTokenResponseHeadKey, String token, String refreshToken);

    void interceptorValid(String tokenResponseHeadKey);

    void validToken(String token);

    void validExpiredToken(String token);

    void validRefreshToken(String refreshToken);

    void generateNewToken(String tokenResponseHeadKey);
}
