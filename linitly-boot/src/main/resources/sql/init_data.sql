INSERT INTO sys_admin_user(id, username, mobile_number, salt, password, job_number, nick_name, head_img_url, created_user_id, last_modified_user_id) VALUES (1, 'admin', '18888888888', 'QeIuiiKS9ZA2KyJuRL9PjKAsMh5P0PC1', 'fe655fa50daa232d9d26b4c97b3fc0bd', '2013205021', 'admin', 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png', 0, 0);

INSERT INTO sys_role(id, name, code, created_user_id, last_modified_user_id) VALUES (1, '管理员', 'admin', 0, 0);

INSERT INTO sys_admin_user_role(admin_user_id, role_id) VALUES (1, 1);

INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (1, '首页', '/home', 'icon-home', 0, 0, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (2, '系统管理', '', 'icon-system', 0, 7, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (3, '用户管理', '/home/user', 'icon-user', 2, 0, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (4, '角色管理', '/home/role', 'icon-role', 2, 0, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (5, '部门管理', '/home/dept', 'icon-department', 2, 0, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (6, '岗位管理', '/home/post', 'icon-gangwei', 2, 0, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (7, '词典管理', '/home/dict', 'icon-dict', 2, 0, 0, 0);
INSERT INTO sys_menu(id, name, url, icon, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (8, '任务调度', '/home/job', 'icon-jobtask', 2, 0, 0, 0);

INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 1);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 2);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 3);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 4);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 5);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 6);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 7);
INSERT INTO sys_role_menu(role_id, menu_id) VALUES (1, 8);

-- 后台用户相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (1, '查询用户列表', 'user:search', 3, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (2, '添加用户', 'user:add', 3, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (3, '修改用户', 'user:update', 3, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (4, '删除用户', 'user:delete', 3, 0, 0);
-- 角色相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (5, '查询角色列表', 'role:search', 4, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (6, '添加角色', 'role:add', 4, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (7, '修改角色', 'role:update', 4, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (8, '为角色赋权', 'role:empower', 4, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (9, '删除角色', 'role:delete', 4, 0, 0);
-- 部门相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (10, '查询部门列表', 'dept:search', 5, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (11, '添加部门', 'dept:add', 5, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (12, '修改部门', 'dept:update', 5, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (13, '删除部门', 'dept:delete', 5, 0, 0);
-- 岗位相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (14, '查询岗位列表', 'post:search', 6, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (15, '添加岗位', 'post:add', 6, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (16, '修改岗位', 'post:update', 6, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (17, '删除岗位', 'post:delete', 6, 0, 0);
-- 词典相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (18, '查询词典列表', 'dict:search', 7, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (19, '添加词典', 'dict:add', 7, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (20, '修改词典', 'dict:update', 7, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (21, '删除词典', 'dict:delete', 7, 0, 0);
-- 词典项相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (22, '查看词典项', 'dict:item:search', 7, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (23, '添加词典项', 'dict:item:add', 7, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (24, '修改词典项', 'dict:item:update', 7, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (25, '删除词典项', 'dict:item:delete', 7, 0, 0);
-- 任务调度相关功能权限
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (26, '查询任务列表', 'job:search', 8, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (27, '添加任务', 'job:add', 8, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (28, '修改任务', 'job:update', 8, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (29, '启动/暂停任务', 'job:start/pause', 8, 0, 0);
INSERT INTO sys_function_permission(id, name, code, sys_menu_id, created_user_id, last_modified_user_id) VALUES (30, '删除任务', 'job:delete', 8, 0, 0);

INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 1);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 2);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 3);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 4);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 5);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 6);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 7);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 8);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 9);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 10);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 11);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 12);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 13);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 14);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 15);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 16);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 17);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 18);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 19);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 20);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 21);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 22);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 23);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 24);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 25);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 26);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 27);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 28);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 29);
INSERT INTO sys_role_function_permission(role_id, function_permission_id) VALUES (1, 30);

INSERT INTO sys_dept(id, name, parent_id, child_number, created_user_id, last_modified_user_id) VALUES (1, '总部门', 0, 0, 0, 0);

INSERT INTO sys_post(id, name, sys_dept_id, created_user_id, last_modified_user_id) VALUES (1, '总部门经理', 1, 0, 0);

INSERT INTO sys_data_dict(id, name, code, created_user_id, last_modified_user_id) VALUES (1, '启用禁用状态', 'enabled', 0, 0);
INSERT INTO sys_data_dict(id, name, code, created_user_id, last_modified_user_id) VALUES (2, '定时任务状态', 'job_status', 0, 0);
INSERT INTO sys_data_dict(id, name, code, created_user_id, last_modified_user_id) VALUES (3, '性别', 'sex', 0, 0);

INSERT INTO sys_data_dict_item(id, value, text, sort, sys_data_dict_id, created_user_id, last_modified_user_id) VALUES (1, '1', '启用', 1, 1, 0, 0);
INSERT INTO sys_data_dict_item(id, value, text, sort, sys_data_dict_id, created_user_id, last_modified_user_id) VALUES (2, '2', '禁用', 2, 1, 0, 0);
INSERT INTO sys_data_dict_item(id, value, text, sort, sys_data_dict_id, created_user_id, last_modified_user_id) VALUES (3, '1', '运行中', 1, 2, 0, 0);
INSERT INTO sys_data_dict_item(id, value, text, sort, sys_data_dict_id, created_user_id, last_modified_user_id) VALUES (4, '2', '已暂停', 1, 2, 0, 0);
INSERT INTO sys_data_dict_item(id, value, text, sort, sys_data_dict_id, created_user_id, last_modified_user_id) VALUES (5, '1', '男', 1, 3, 0, 0);
INSERT INTO sys_data_dict_item(id, value, text, sort, sys_data_dict_id, created_user_id, last_modified_user_id) VALUES (6, '2', '女', 1, 3, 0, 0);