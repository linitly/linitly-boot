package org.linitly.business.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.helper.entity.BaseVO;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序账单信息表VO")
public class MiniBillVO extends BaseVO {

    @ApiModelProperty(value = "金额")
    private Long amount;

    @ApiModelProperty(value = "账单类型(1:支出;2:收入;)")
    private Integer type;

    @ApiModelProperty(value = "账单分类id")
    private Long billTypeId;

    @ApiModelProperty(value = "账单消费时间")
    private Date time;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "自然人id")
    private Long naturePersonId;
}