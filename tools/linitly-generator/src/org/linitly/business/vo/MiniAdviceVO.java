package org.linitly.business.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.helper.entity.BaseVO;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序建议表VO")
public class MiniAdviceVO extends BaseVO {

    @ApiModelProperty(value = "建议标题")
    private String title;

    @ApiModelProperty(value = "建议详情")
    private String detail;

    @ApiModelProperty(value = "是否查看(0:未查看;1:已查看;)")
    private Integer viewed;

    @ApiModelProperty(value = "是否有回复(0:未回复;1:已回复;)")
    private Integer hasReply;

    @ApiModelProperty(value = "回复内容")
    private String reply;

    @ApiModelProperty(value = "自然人id")
    private Long naturePersonId;
}