package org.linitly.business.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.helper.entity.BaseVO;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序账号信息表VO")
public class MiniAccountVO extends BaseVO {

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "二级密码")
    private String twoPwd;

    @ApiModelProperty(value = "账号分类")
    private Integer accountType;

    @ApiModelProperty(value = "账号地址")
    private String address;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "自然人id")
    private Long naturePersonId;
}