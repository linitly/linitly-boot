package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniAdvice;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniAdviceMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniAdvice miniAdvice);

    MiniAdvice findById(Long id);

    int updateById(MiniAdvice miniAdvice);

    List<MiniAdvice> findAll(MiniAdvice miniAdvice);

    int insertSelective(MiniAdvice miniAdvice);

    int updateByIdSelective(MiniAdvice miniAdvice);
}