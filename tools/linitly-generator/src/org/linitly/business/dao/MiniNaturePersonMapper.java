package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniNaturePerson;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniNaturePersonMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniNaturePerson miniNaturePerson);

    MiniNaturePerson findById(Long id);

    int updateById(MiniNaturePerson miniNaturePerson);

    List<MiniNaturePerson> findAll(MiniNaturePerson miniNaturePerson);

    int insertSelective(MiniNaturePerson miniNaturePerson);

    int updateByIdSelective(MiniNaturePerson miniNaturePerson);
}