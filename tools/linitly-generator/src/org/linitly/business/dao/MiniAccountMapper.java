package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniAccount;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniAccountMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniAccount miniAccount);

    MiniAccount findById(Long id);

    int updateById(MiniAccount miniAccount);

    List<MiniAccount> findAll(MiniAccount miniAccount);

    int insertSelective(MiniAccount miniAccount);

    int updateByIdSelective(MiniAccount miniAccount);
}