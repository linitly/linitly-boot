package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniUser;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniUserMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniUser miniUser);

    MiniUser findById(Long id);

    int updateById(MiniUser miniUser);

    List<MiniUser> findAll(MiniUser miniUser);

    int insertSelective(MiniUser miniUser);

    int updateByIdSelective(MiniUser miniUser);
}