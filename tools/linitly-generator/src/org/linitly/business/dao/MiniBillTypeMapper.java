package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniBillType;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniBillTypeMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniBillType miniBillType);

    MiniBillType findById(Long id);

    int updateById(MiniBillType miniBillType);

    List<MiniBillType> findAll(MiniBillType miniBillType);

    int insertSelective(MiniBillType miniBillType);

    int updateByIdSelective(MiniBillType miniBillType);
}