package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniBill;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniBillMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniBill miniBill);

    MiniBill findById(Long id);

    int updateById(MiniBill miniBill);

    List<MiniBill> findAll(MiniBill miniBill);

    int insertSelective(MiniBill miniBill);

    int updateByIdSelective(MiniBill miniBill);
}