package org.linitly.business.dao;

import java.util.List;
import org.linitly.boot.base.annotation.DeleteBackup;
import org.linitly.business.entity.MiniStatistical;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniStatisticalMapper {

    @DeleteBackup
    int deleteById(Long id);

    int insert(MiniStatistical miniStatistical);

    MiniStatistical findById(Long id);

    int updateById(MiniStatistical miniStatistical);

    List<MiniStatistical> findAll(MiniStatistical miniStatistical);

    int insertSelective(MiniStatistical miniStatistical);

    int updateByIdSelective(MiniStatistical miniStatistical);
}