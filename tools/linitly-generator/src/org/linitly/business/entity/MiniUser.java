package org.linitly.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.helper.entity.BaseEntity;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序用户表")
public class MiniUser extends BaseEntity {

    @ApiModelProperty(value = "小程序openid")
    private String openid;
}