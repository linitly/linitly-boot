package org.linitly.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.helper.entity.BaseEntity;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序统计信息表")
public class MiniStatistical extends BaseEntity {

    @ApiModelProperty(value = "昨日新增小程序用户数")
    private Integer miniUserAdd;

    @ApiModelProperty(value = "昨日新增自然人数")
    private Integer natureUserAdd;

    @ApiModelProperty(value = "昨日新增账单数量")
    private Integer billAdd;

    @ApiModelProperty(value = "昨日新增账号数")
    private Integer accountAdd;

    @ApiModelProperty(value = "昨日新增建议数")
    private Integer adviceAdd;
}