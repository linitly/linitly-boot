package org.linitly.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.linitly.boot.base.helper.entity.BaseEntity;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序账单分类表")
public class MiniBillType extends BaseEntity {

    @ApiModelProperty(value = "分类图标")
    private String icon;

    @ApiModelProperty(value = "分类名")
    private String name;

    @ApiModelProperty(value = "分类类型(1:支出;2:收入;)")
    private Integer type;
}