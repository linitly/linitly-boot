package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniUserConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序用户表DTO")
public class MiniUserDTO extends BaseDTO {

    @ApiModelProperty(value = "小程序openid", required = true)
    @NotBlank(message = MiniUserConstant.OPENID_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniUserConstant.MAX_OPENID_SIZE, message = MiniUserConstant.OPENID_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String openid;
}