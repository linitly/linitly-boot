package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniNaturePersonConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "自然人表DTO")
public class MiniNaturePersonDTO extends BaseDTO {

    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = MiniNaturePersonConstant.MOBILE_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniNaturePersonConstant.MAX_MOBILE_SIZE, message = MiniNaturePersonConstant.MOBILE_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String mobile;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = MiniNaturePersonConstant.PWD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniNaturePersonConstant.MAX_PWD_SIZE, message = MiniNaturePersonConstant.PWD_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String pwd;

    @ApiModelProperty(value = "密钥(用于前后端的对称加密)", required = true)
    @NotBlank(message = MiniNaturePersonConstant.KEY_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniNaturePersonConstant.MAX_KEY_SIZE, message = MiniNaturePersonConstant.KEY_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String key;
}