package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniBillTypeConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序账单分类表DTO")
public class MiniBillTypeDTO extends BaseDTO {

    @ApiModelProperty(value = "分类图标", required = true)
    @NotBlank(message = MiniBillTypeConstant.ICON_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniBillTypeConstant.MAX_ICON_SIZE, message = MiniBillTypeConstant.ICON_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String icon;

    @ApiModelProperty(value = "分类名", required = true)
    @NotBlank(message = MiniBillTypeConstant.NAME_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniBillTypeConstant.MAX_NAME_SIZE, message = MiniBillTypeConstant.NAME_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String name;

    @ApiModelProperty(value = "分类类型(1:支出;2:收入;)", required = true)
    @NotNull(message = MiniBillTypeConstant.TYPE_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniBillTypeConstant.TYPE_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer type;
}