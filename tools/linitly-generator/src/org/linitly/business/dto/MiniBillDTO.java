package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniBillConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序账单信息表DTO")
public class MiniBillDTO extends BaseDTO {

    @ApiModelProperty(value = "金额", required = true)
    @NotNull(message = MiniBillConstant.AMOUNT_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniBillConstant.AMOUNT_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Long amount;

    @ApiModelProperty(value = "账单类型(1:支出;2:收入;)", required = true)
    @NotNull(message = MiniBillConstant.TYPE_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniBillConstant.TYPE_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer type;

    @ApiModelProperty(value = "账单分类id", required = true)
    @NotNull(message = MiniBillConstant.BILL_TYPE_ID_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniBillConstant.BILL_TYPE_ID_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Long billTypeId;

    @ApiModelProperty(value = "账单消费时间", required = true)
    private java.util.Date time;

    @ApiModelProperty(value = "备注")
    @Size(max = MiniBillConstant.MAX_REMARK_SIZE, message = MiniBillConstant.REMARK_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String remark;

    @ApiModelProperty(value = "自然人id", required = true)
    @NotNull(message = MiniBillConstant.NATURE_PERSON_ID_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniBillConstant.NATURE_PERSON_ID_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Long naturePersonId;
}