package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniAdviceConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序建议表DTO")
public class MiniAdviceDTO extends BaseDTO {

    @ApiModelProperty(value = "建议标题", required = true)
    @NotBlank(message = MiniAdviceConstant.TITLE_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniAdviceConstant.MAX_TITLE_SIZE, message = MiniAdviceConstant.TITLE_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String title;

    @ApiModelProperty(value = "建议详情")
    @Size(max = MiniAdviceConstant.MAX_DETAIL_SIZE, message = MiniAdviceConstant.DETAIL_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String detail;

    @ApiModelProperty(value = "是否查看(0:未查看;1:已查看;)", required = true)
    @NotNull(message = MiniAdviceConstant.VIEWED_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniAdviceConstant.VIEWED_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer viewed;

    @ApiModelProperty(value = "是否有回复(0:未回复;1:已回复;)", required = true)
    @NotNull(message = MiniAdviceConstant.HAS_REPLY_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniAdviceConstant.HAS_REPLY_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer hasReply;

    @ApiModelProperty(value = "回复内容")
    @Size(max = MiniAdviceConstant.MAX_REPLY_SIZE, message = MiniAdviceConstant.REPLY_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String reply;

    @ApiModelProperty(value = "自然人id", required = true)
    @NotNull(message = MiniAdviceConstant.NATURE_PERSON_ID_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniAdviceConstant.NATURE_PERSON_ID_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Long naturePersonId;
}