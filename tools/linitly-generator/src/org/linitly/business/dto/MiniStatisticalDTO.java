package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniStatisticalConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序统计信息表DTO")
public class MiniStatisticalDTO extends BaseDTO {

    @ApiModelProperty(value = "昨日新增小程序用户数", required = true)
    @NotNull(message = MiniStatisticalConstant.MINI_USER_ADD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniStatisticalConstant.MINI_USER_ADD_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer miniUserAdd;

    @ApiModelProperty(value = "昨日新增自然人数", required = true)
    @NotNull(message = MiniStatisticalConstant.NATURE_USER_ADD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniStatisticalConstant.NATURE_USER_ADD_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer natureUserAdd;

    @ApiModelProperty(value = "昨日新增账单数量", required = true)
    @NotNull(message = MiniStatisticalConstant.BILL_ADD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniStatisticalConstant.BILL_ADD_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer billAdd;

    @ApiModelProperty(value = "昨日新增账号数", required = true)
    @NotNull(message = MiniStatisticalConstant.ACCOUNT_ADD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniStatisticalConstant.ACCOUNT_ADD_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer accountAdd;

    @ApiModelProperty(value = "昨日新增建议数", required = true)
    @NotNull(message = MiniStatisticalConstant.ADVICE_ADD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniStatisticalConstant.ADVICE_ADD_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer adviceAdd;
}