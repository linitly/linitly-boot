package org.linitly.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.linitly.boot.base.helper.entity.BaseDTO;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.constant.MiniAccountConstant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "小程序账号信息表DTO")
public class MiniAccountDTO extends BaseDTO {

    @ApiModelProperty(value = "标题", required = true)
    @NotBlank(message = MiniAccountConstant.TITLE_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniAccountConstant.MAX_TITLE_SIZE, message = MiniAccountConstant.TITLE_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String title;

    @ApiModelProperty(value = "账号", required = true)
    @NotBlank(message = MiniAccountConstant.ACCOUNT_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniAccountConstant.MAX_ACCOUNT_SIZE, message = MiniAccountConstant.ACCOUNT_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String account;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = MiniAccountConstant.PASSWORD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniAccountConstant.MAX_PASSWORD_SIZE, message = MiniAccountConstant.PASSWORD_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String password;

    @ApiModelProperty(value = "二级密码", required = true)
    @NotBlank(message = MiniAccountConstant.TWO_PWD_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Size(max = MiniAccountConstant.MAX_TWO_PWD_SIZE, message = MiniAccountConstant.TWO_PWD_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String twoPwd;

    @ApiModelProperty(value = "账号分类", required = true)
    @NotNull(message = MiniAccountConstant.ACCOUNT_TYPE_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniAccountConstant.ACCOUNT_TYPE_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Integer accountType;

    @ApiModelProperty(value = "账号地址")
    @Size(max = MiniAccountConstant.MAX_ADDRESS_SIZE, message = MiniAccountConstant.ADDRESS_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String address;

    @ApiModelProperty(value = "备注")
    @Size(max = MiniAccountConstant.MAX_REMARK_SIZE, message = MiniAccountConstant.REMARK_SIZE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private String remark;

    @ApiModelProperty(value = "自然人id", required = true)
    @NotNull(message = MiniAccountConstant.NATURE_PERSON_ID_EMPTY_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    @Range(message = MiniAccountConstant.NATURE_PERSON_ID_RANGE_ERROR, groups = {InsertValidGroup.class, UpdateValidGroup.class})
    private Long naturePersonId;
}