package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniNaturePersonConstant {

    String MOBILE_EMPTY_ERROR = "手机号不能为空";

    int MAX_MOBILE_SIZE = 16;

    String MOBILE_SIZE_ERROR = "手机号长度不符合限制";

    String PWD_EMPTY_ERROR = "密码不能为空";

    int MAX_PWD_SIZE = 6;

    String PWD_SIZE_ERROR = "密码长度不符合限制";

    String KEY_EMPTY_ERROR = "密钥(用于前后端的对称加密)不能为空";

    int MAX_KEY_SIZE = 16;

    String KEY_SIZE_ERROR = "密钥(用于前后端的对称加密)长度不符合限制";
}