package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniStatisticalConstant {

    String MINI_USER_ADD_EMPTY_ERROR = "昨日新增小程序用户数不能为空";

    String MINI_USER_ADD_RANGE_ERROR = "昨日新增小程序用户数大小不符合限制";

    String NATURE_USER_ADD_EMPTY_ERROR = "昨日新增自然人数不能为空";

    String NATURE_USER_ADD_RANGE_ERROR = "昨日新增自然人数大小不符合限制";

    String BILL_ADD_EMPTY_ERROR = "昨日新增账单数量不能为空";

    String BILL_ADD_RANGE_ERROR = "昨日新增账单数量大小不符合限制";

    String ACCOUNT_ADD_EMPTY_ERROR = "昨日新增账号数不能为空";

    String ACCOUNT_ADD_RANGE_ERROR = "昨日新增账号数大小不符合限制";

    String ADVICE_ADD_EMPTY_ERROR = "昨日新增建议数不能为空";

    String ADVICE_ADD_RANGE_ERROR = "昨日新增建议数大小不符合限制";
}