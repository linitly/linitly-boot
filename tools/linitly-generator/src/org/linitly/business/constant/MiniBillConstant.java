package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniBillConstant {

    String AMOUNT_EMPTY_ERROR = "金额不能为空";

    String AMOUNT_RANGE_ERROR = "金额大小不符合限制";

    String TYPE_EMPTY_ERROR = "账单类型(1:支出;2:收入;)不能为空";

    String TYPE_RANGE_ERROR = "账单类型(1:支出;2:收入;)大小不符合限制";

    String BILL_TYPE_ID_EMPTY_ERROR = "账单分类id不能为空";

    String BILL_TYPE_ID_RANGE_ERROR = "账单分类id大小不符合限制";

    int MAX_REMARK_SIZE = 30;

    String REMARK_SIZE_ERROR = "备注长度不符合限制";

    String NATURE_PERSON_ID_EMPTY_ERROR = "自然人id不能为空";

    String NATURE_PERSON_ID_RANGE_ERROR = "自然人id大小不符合限制";
}