package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniBillTypeConstant {

    String ICON_EMPTY_ERROR = "分类图标不能为空";

    int MAX_ICON_SIZE = 32;

    String ICON_SIZE_ERROR = "分类图标长度不符合限制";

    String NAME_EMPTY_ERROR = "分类名不能为空";

    int MAX_NAME_SIZE = 4;

    String NAME_SIZE_ERROR = "分类名长度不符合限制";

    String TYPE_EMPTY_ERROR = "分类类型(1:支出;2:收入;)不能为空";

    String TYPE_RANGE_ERROR = "分类类型(1:支出;2:收入;)大小不符合限制";
}