package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniAdviceConstant {

    String TITLE_EMPTY_ERROR = "建议标题不能为空";

    int MAX_TITLE_SIZE = 64;

    String TITLE_SIZE_ERROR = "建议标题长度不符合限制";

    int MAX_DETAIL_SIZE = 500;

    String DETAIL_SIZE_ERROR = "建议详情长度不符合限制";

    String VIEWED_EMPTY_ERROR = "是否查看(0:未查看;1:已查看;)不能为空";

    String VIEWED_RANGE_ERROR = "是否查看(0:未查看;1:已查看;)大小不符合限制";

    String HAS_REPLY_EMPTY_ERROR = "是否有回复(0:未回复;1:已回复;)不能为空";

    String HAS_REPLY_RANGE_ERROR = "是否有回复(0:未回复;1:已回复;)大小不符合限制";

    int MAX_REPLY_SIZE = 500;

    String REPLY_SIZE_ERROR = "回复内容长度不符合限制";

    String NATURE_PERSON_ID_EMPTY_ERROR = "自然人id不能为空";

    String NATURE_PERSON_ID_RANGE_ERROR = "自然人id大小不符合限制";
}