package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniAccountConstant {

    String TITLE_EMPTY_ERROR = "标题不能为空";

    int MAX_TITLE_SIZE = 64;

    String TITLE_SIZE_ERROR = "标题长度不符合限制";

    String ACCOUNT_EMPTY_ERROR = "账号不能为空";

    int MAX_ACCOUNT_SIZE = 64;

    String ACCOUNT_SIZE_ERROR = "账号长度不符合限制";

    String PASSWORD_EMPTY_ERROR = "密码不能为空";

    int MAX_PASSWORD_SIZE = 64;

    String PASSWORD_SIZE_ERROR = "密码长度不符合限制";

    String TWO_PWD_EMPTY_ERROR = "二级密码不能为空";

    int MAX_TWO_PWD_SIZE = 64;

    String TWO_PWD_SIZE_ERROR = "二级密码长度不符合限制";

    String ACCOUNT_TYPE_EMPTY_ERROR = "账号分类不能为空";

    String ACCOUNT_TYPE_RANGE_ERROR = "账号分类大小不符合限制";

    int MAX_ADDRESS_SIZE = 128;

    String ADDRESS_SIZE_ERROR = "账号地址长度不符合限制";

    int MAX_REMARK_SIZE = 255;

    String REMARK_SIZE_ERROR = "备注长度不符合限制";

    String NATURE_PERSON_ID_EMPTY_ERROR = "自然人id不能为空";

    String NATURE_PERSON_ID_RANGE_ERROR = "自然人id大小不符合限制";
}