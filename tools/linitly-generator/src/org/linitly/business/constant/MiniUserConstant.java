package org.linitly.business.constant;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
public interface MiniUserConstant {

    String OPENID_EMPTY_ERROR = "小程序openid不能为空";

    int MAX_OPENID_SIZE = 128;

    String OPENID_SIZE_ERROR = "小程序openid长度不符合限制";
}