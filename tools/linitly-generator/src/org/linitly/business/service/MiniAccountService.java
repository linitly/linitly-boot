package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniAccountMapper;
import org.linitly.business.dto.MiniAccountDTO;
import org.linitly.business.entity.MiniAccount;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniAccountService {

    @Autowired
    private MiniAccountMapper miniAccountMapper;

    public void insert(MiniAccountDTO dto) {
        MiniAccount miniAccount = new MiniAccount();
        BeanUtils.copyProperties(dto, miniAccount);
        miniAccountMapper.insertSelective(miniAccount);
    }

    public void updateById(MiniAccountDTO dto) {
        MiniAccount miniAccount = new MiniAccount();
        BeanUtils.copyProperties(dto, miniAccount);
        miniAccountMapper.updateByIdSelective(miniAccount);
    }

    public MiniAccount findById(Long id) {
        return miniAccountMapper.findById(id);
    }

    public List<MiniAccount> findAll(MiniAccount miniAccount) {
        return miniAccountMapper.findAll(miniAccount);
    }

    public void deleteById(Long id) {
        miniAccountMapper.deleteById(id);
    }
}