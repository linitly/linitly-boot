package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniUserMapper;
import org.linitly.business.dto.MiniUserDTO;
import org.linitly.business.entity.MiniUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniUserService {

    @Autowired
    private MiniUserMapper miniUserMapper;

    public void insert(MiniUserDTO dto) {
        MiniUser miniUser = new MiniUser();
        BeanUtils.copyProperties(dto, miniUser);
        miniUserMapper.insertSelective(miniUser);
    }

    public void updateById(MiniUserDTO dto) {
        MiniUser miniUser = new MiniUser();
        BeanUtils.copyProperties(dto, miniUser);
        miniUserMapper.updateByIdSelective(miniUser);
    }

    public MiniUser findById(Long id) {
        return miniUserMapper.findById(id);
    }

    public List<MiniUser> findAll(MiniUser miniUser) {
        return miniUserMapper.findAll(miniUser);
    }

    public void deleteById(Long id) {
        miniUserMapper.deleteById(id);
    }
}