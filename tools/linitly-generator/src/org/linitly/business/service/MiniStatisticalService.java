package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniStatisticalMapper;
import org.linitly.business.dto.MiniStatisticalDTO;
import org.linitly.business.entity.MiniStatistical;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniStatisticalService {

    @Autowired
    private MiniStatisticalMapper miniStatisticalMapper;

    public void insert(MiniStatisticalDTO dto) {
        MiniStatistical miniStatistical = new MiniStatistical();
        BeanUtils.copyProperties(dto, miniStatistical);
        miniStatisticalMapper.insertSelective(miniStatistical);
    }

    public void updateById(MiniStatisticalDTO dto) {
        MiniStatistical miniStatistical = new MiniStatistical();
        BeanUtils.copyProperties(dto, miniStatistical);
        miniStatisticalMapper.updateByIdSelective(miniStatistical);
    }

    public MiniStatistical findById(Long id) {
        return miniStatisticalMapper.findById(id);
    }

    public List<MiniStatistical> findAll(MiniStatistical miniStatistical) {
        return miniStatisticalMapper.findAll(miniStatistical);
    }

    public void deleteById(Long id) {
        miniStatisticalMapper.deleteById(id);
    }
}