package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniAdviceMapper;
import org.linitly.business.dto.MiniAdviceDTO;
import org.linitly.business.entity.MiniAdvice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniAdviceService {

    @Autowired
    private MiniAdviceMapper miniAdviceMapper;

    public void insert(MiniAdviceDTO dto) {
        MiniAdvice miniAdvice = new MiniAdvice();
        BeanUtils.copyProperties(dto, miniAdvice);
        miniAdviceMapper.insertSelective(miniAdvice);
    }

    public void updateById(MiniAdviceDTO dto) {
        MiniAdvice miniAdvice = new MiniAdvice();
        BeanUtils.copyProperties(dto, miniAdvice);
        miniAdviceMapper.updateByIdSelective(miniAdvice);
    }

    public MiniAdvice findById(Long id) {
        return miniAdviceMapper.findById(id);
    }

    public List<MiniAdvice> findAll(MiniAdvice miniAdvice) {
        return miniAdviceMapper.findAll(miniAdvice);
    }

    public void deleteById(Long id) {
        miniAdviceMapper.deleteById(id);
    }
}