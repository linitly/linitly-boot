package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniBillTypeMapper;
import org.linitly.business.dto.MiniBillTypeDTO;
import org.linitly.business.entity.MiniBillType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniBillTypeService {

    @Autowired
    private MiniBillTypeMapper miniBillTypeMapper;

    public void insert(MiniBillTypeDTO dto) {
        MiniBillType miniBillType = new MiniBillType();
        BeanUtils.copyProperties(dto, miniBillType);
        miniBillTypeMapper.insertSelective(miniBillType);
    }

    public void updateById(MiniBillTypeDTO dto) {
        MiniBillType miniBillType = new MiniBillType();
        BeanUtils.copyProperties(dto, miniBillType);
        miniBillTypeMapper.updateByIdSelective(miniBillType);
    }

    public MiniBillType findById(Long id) {
        return miniBillTypeMapper.findById(id);
    }

    public List<MiniBillType> findAll(MiniBillType miniBillType) {
        return miniBillTypeMapper.findAll(miniBillType);
    }

    public void deleteById(Long id) {
        miniBillTypeMapper.deleteById(id);
    }
}