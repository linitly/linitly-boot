package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniBillMapper;
import org.linitly.business.dto.MiniBillDTO;
import org.linitly.business.entity.MiniBill;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniBillService {

    @Autowired
    private MiniBillMapper miniBillMapper;

    public void insert(MiniBillDTO dto) {
        MiniBill miniBill = new MiniBill();
        BeanUtils.copyProperties(dto, miniBill);
        miniBillMapper.insertSelective(miniBill);
    }

    public void updateById(MiniBillDTO dto) {
        MiniBill miniBill = new MiniBill();
        BeanUtils.copyProperties(dto, miniBill);
        miniBillMapper.updateByIdSelective(miniBill);
    }

    public MiniBill findById(Long id) {
        return miniBillMapper.findById(id);
    }

    public List<MiniBill> findAll(MiniBill miniBill) {
        return miniBillMapper.findAll(miniBill);
    }

    public void deleteById(Long id) {
        miniBillMapper.deleteById(id);
    }
}