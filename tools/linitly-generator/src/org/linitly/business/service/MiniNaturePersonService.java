package org.linitly.business.service;

import java.util.List;
import org.linitly.business.dao.MiniNaturePersonMapper;
import org.linitly.business.dto.MiniNaturePersonDTO;
import org.linitly.business.entity.MiniNaturePerson;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Service
public class MiniNaturePersonService {

    @Autowired
    private MiniNaturePersonMapper miniNaturePersonMapper;

    public void insert(MiniNaturePersonDTO dto) {
        MiniNaturePerson miniNaturePerson = new MiniNaturePerson();
        BeanUtils.copyProperties(dto, miniNaturePerson);
        miniNaturePersonMapper.insertSelective(miniNaturePerson);
    }

    public void updateById(MiniNaturePersonDTO dto) {
        MiniNaturePerson miniNaturePerson = new MiniNaturePerson();
        BeanUtils.copyProperties(dto, miniNaturePerson);
        miniNaturePersonMapper.updateByIdSelective(miniNaturePerson);
    }

    public MiniNaturePerson findById(Long id) {
        return miniNaturePersonMapper.findById(id);
    }

    public List<MiniNaturePerson> findAll(MiniNaturePerson miniNaturePerson) {
        return miniNaturePersonMapper.findAll(miniNaturePerson);
    }

    public void deleteById(Long id) {
        miniNaturePersonMapper.deleteById(id);
    }
}