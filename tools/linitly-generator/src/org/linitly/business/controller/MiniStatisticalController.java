package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniStatisticalDTO;
import org.linitly.business.entity.MiniStatistical;
import org.linitly.business.service.MiniStatisticalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniStatistical")
@Api(tags = "小程序统计信息表管理")
public class MiniStatisticalController {

    @Autowired
    private MiniStatisticalService miniStatisticalService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加小程序统计信息表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniStatisticalDTO dto, BindingResult bindingResult) {
        miniStatisticalService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改小程序统计信息表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniStatisticalDTO dto, BindingResult bindingResult) {
        miniStatisticalService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询小程序统计信息表")
    public MiniStatistical findById(@PathVariable Long id) {
        return miniStatisticalService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询小程序统计信息表列表")
    public List<MiniStatistical> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniStatistical miniStatistical) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniStatisticalService.findAll(miniStatistical);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除小程序统计信息表")
    public void deleteById(@PathVariable Long id) {
        miniStatisticalService.deleteById(id);
    }
}