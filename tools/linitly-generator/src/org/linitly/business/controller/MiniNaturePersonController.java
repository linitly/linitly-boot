package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniNaturePersonDTO;
import org.linitly.business.entity.MiniNaturePerson;
import org.linitly.business.service.MiniNaturePersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniNaturePerson")
@Api(tags = "自然人表管理")
public class MiniNaturePersonController {

    @Autowired
    private MiniNaturePersonService miniNaturePersonService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加自然人表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniNaturePersonDTO dto, BindingResult bindingResult) {
        miniNaturePersonService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改自然人表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniNaturePersonDTO dto, BindingResult bindingResult) {
        miniNaturePersonService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询自然人表")
    public MiniNaturePerson findById(@PathVariable Long id) {
        return miniNaturePersonService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询自然人表列表")
    public List<MiniNaturePerson> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniNaturePerson miniNaturePerson) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniNaturePersonService.findAll(miniNaturePerson);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除自然人表")
    public void deleteById(@PathVariable Long id) {
        miniNaturePersonService.deleteById(id);
    }
}