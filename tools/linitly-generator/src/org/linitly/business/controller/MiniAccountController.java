package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniAccountDTO;
import org.linitly.business.entity.MiniAccount;
import org.linitly.business.service.MiniAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniAccount")
@Api(tags = "小程序账号信息表管理")
public class MiniAccountController {

    @Autowired
    private MiniAccountService miniAccountService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加小程序账号信息表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniAccountDTO dto, BindingResult bindingResult) {
        miniAccountService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改小程序账号信息表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniAccountDTO dto, BindingResult bindingResult) {
        miniAccountService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询小程序账号信息表")
    public MiniAccount findById(@PathVariable Long id) {
        return miniAccountService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询小程序账号信息表列表")
    public List<MiniAccount> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniAccount miniAccount) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniAccountService.findAll(miniAccount);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除小程序账号信息表")
    public void deleteById(@PathVariable Long id) {
        miniAccountService.deleteById(id);
    }
}