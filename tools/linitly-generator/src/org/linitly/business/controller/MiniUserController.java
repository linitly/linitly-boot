package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniUserDTO;
import org.linitly.business.entity.MiniUser;
import org.linitly.business.service.MiniUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniUser")
@Api(tags = "小程序用户表管理")
public class MiniUserController {

    @Autowired
    private MiniUserService miniUserService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加小程序用户表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniUserDTO dto, BindingResult bindingResult) {
        miniUserService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改小程序用户表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniUserDTO dto, BindingResult bindingResult) {
        miniUserService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询小程序用户表")
    public MiniUser findById(@PathVariable Long id) {
        return miniUserService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询小程序用户表列表")
    public List<MiniUser> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniUser miniUser) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniUserService.findAll(miniUser);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除小程序用户表")
    public void deleteById(@PathVariable Long id) {
        miniUserService.deleteById(id);
    }
}