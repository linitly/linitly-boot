package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniAdviceDTO;
import org.linitly.business.entity.MiniAdvice;
import org.linitly.business.service.MiniAdviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniAdvice")
@Api(tags = "小程序建议表管理")
public class MiniAdviceController {

    @Autowired
    private MiniAdviceService miniAdviceService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加小程序建议表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniAdviceDTO dto, BindingResult bindingResult) {
        miniAdviceService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改小程序建议表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniAdviceDTO dto, BindingResult bindingResult) {
        miniAdviceService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询小程序建议表")
    public MiniAdvice findById(@PathVariable Long id) {
        return miniAdviceService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询小程序建议表列表")
    public List<MiniAdvice> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniAdvice miniAdvice) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniAdviceService.findAll(miniAdvice);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除小程序建议表")
    public void deleteById(@PathVariable Long id) {
        miniAdviceService.deleteById(id);
    }
}