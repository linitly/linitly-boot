package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniBillTypeDTO;
import org.linitly.business.entity.MiniBillType;
import org.linitly.business.service.MiniBillTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniBillType")
@Api(tags = "小程序账单分类表管理")
public class MiniBillTypeController {

    @Autowired
    private MiniBillTypeService miniBillTypeService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加小程序账单分类表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniBillTypeDTO dto, BindingResult bindingResult) {
        miniBillTypeService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改小程序账单分类表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniBillTypeDTO dto, BindingResult bindingResult) {
        miniBillTypeService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询小程序账单分类表")
    public MiniBillType findById(@PathVariable Long id) {
        return miniBillTypeService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询小程序账单分类表列表")
    public List<MiniBillType> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniBillType miniBillType) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniBillTypeService.findAll(miniBillType);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除小程序账单分类表")
    public void deleteById(@PathVariable Long id) {
        miniBillTypeService.deleteById(id);
    }
}