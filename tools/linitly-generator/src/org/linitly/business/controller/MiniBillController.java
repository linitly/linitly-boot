package org.linitly.business.controller;

import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.linitly.boot.base.annotation.Pagination;
import org.linitly.boot.base.annotation.Result;
import org.linitly.boot.base.constant.admin.AdminCommonConstant;
import org.linitly.boot.base.helper.groups.InsertValidGroup;
import org.linitly.boot.base.helper.groups.UpdateValidGroup;
import org.linitly.business.dto.MiniBillDTO;
import org.linitly.business.entity.MiniBill;
import org.linitly.business.service.MiniBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: linitly-generator
 * @date: 2022-02-10 11:01
 * @description: 
 */
@Result
@RestController
@RequestMapping("/miniBill")
@Api(tags = "小程序账单信息表管理")
public class MiniBillController {

    @Autowired
    private MiniBillService miniBillService;

    @PostMapping("/insert")
    @ApiOperation(value = "添加小程序账单信息表")
    public void insert(@RequestBody @Validated({InsertValidGroup.class}) MiniBillDTO dto, BindingResult bindingResult) {
        miniBillService.insert(dto);
    }

    @PostMapping("/updateById")
    @ApiOperation(value = "修改小程序账单信息表")
    public void updateById(@RequestBody @Validated({UpdateValidGroup.class}) MiniBillDTO dto, BindingResult bindingResult) {
        miniBillService.updateById(dto);
    }

    @PostMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询小程序账单信息表")
    public MiniBill findById(@PathVariable Long id) {
        return miniBillService.findById(id);
    }

    @Pagination
    @PostMapping("/findAll")
    @ApiOperation(value = "查询小程序账单信息表列表")
    public List<MiniBill> findAll(@RequestParam(defaultValue = AdminCommonConstant.PAGE_NUMBER) int pageNumber, @RequestParam(defaultValue = AdminCommonConstant.PAGE_SIZE) int pageSize, @RequestBody(required = false) MiniBill miniBill) {
        PageHelper.startPage(pageNumber, pageSize, "id desc");
        return miniBillService.findAll(miniBill);
    }

    @PostMapping("/deleteById/{id}")
    @ApiOperation(value = "根据id删除小程序账单信息表")
    public void deleteById(@PathVariable Long id) {
        miniBillService.deleteById(id);
    }
}